{% include fsh-link-references.md %}

This page describes the business scenarios for the **Patient Identification Use Case**.

This scenario can be realized by using a common interaction pattern describing the search and retrieval of a document.

Future specifications may differentiate the cases based on the search paramters used and the kind of data that is supposed to be seaerched and retrieved.

### Scenario

#### Check Patient ID

1. A patient from country A visits a HP in country B seeking for healthcare assistance
1. Patient identifies himself.
1. The HP informs the patient about his/her data protection rights and asks for patient's consent (where applicable). The HP requests the validation of the identity of the patient through the HP interface.
1. The HP interface conveys this request to the NCP of country B
1. The NCP of country B conveys this request to the NCP of country A
1. The NCP of country A checks ID and provides to the NCP of country B the (positive or negative) patient’s identification confirmation.
1. The  NCP  of  country  B  provides  the  patient’s  identity  confirmation  to  the  HP interface



### Diagram 

#### Laboratory Result Report sharing on a cross-border scale

{% include sequence-business-pat.svg %}


<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: click on the diagram to see more details about each interaction 
            <a href="application.html" target="_blank">
                [application view]
            </a>.
        </td>
    </tr>
</table>
</div>

