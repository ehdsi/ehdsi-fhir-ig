{% include fsh-link-references.md %}

The **business domain** describes the goals, business processes and rules of the MyHealth@EU services.


<div>
{% include bdat-b.svg %}
</div>

### Goal

MyHealth@EU aims to facilitate the secure and seamless exchange of health data across European countries, enabling patients and healthcare providers to access essential medical information when and where it is needed.

More contextual information about MyHealth@EU can be found [here](https://health.ec.europa.eu/ehealth-digital-health-and-care/electronic-cross-border-health-services_en/). 


### Use Cases &  Business Scenarios

A description of the MyHealth@EU Use cases and associated Business Scenarios is provided in the [MyHealth@EU requirement catalogue](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/1.+MyHealth@EU+Requirements+Catalogue).


### Business Services

To achieve the goal and use cases described above, MyHealth@EU envisions a set of cross-border business services, as listed below.


|       | Service                          | Status            | Standard  | Details (link) |
|-------|----------------------------------|-------------------|-----------|:------: |
| 📝    | Patient Summary                  | In operation      | HL7 CDA   | X |
| 💊    | ePrescription and eDispensation  | In operation      | HL7 CDA   | X |
| 📄    | Original Clinical Document       | In operation      | HL7 CDA   | X |
| 🔬    | Laboratory Result Report         | Pre-production    | HL7 FHIR  | [Laboratory Report Scenario](bus-scenario-lab.html) |
| 🏥    | Hospital Discharge Report        | Under definition  | HL7 FHIR  | [HDR Scenario](bus-scenario-hdr.html) |
| 🖼️    | Medical Imaging Study            | Under definition  | HL7 FHIR  | To be documented |



<div>
<p> </p>
<p> The Details column provides a link to access the page describing the detailed business scenario</p>
<p> </p>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: not all of these services have the same level of maturity, nor do they all use HL7 FHIR as the reference standard. <br />
            This version of the guide documents only the HL7 FHIR-based services
        </td>
    </tr>
</table>
<p> </p>
</div>



These services make use of the Patient Identification Service for which two implementations are available.

|       | Service                          | Status            | Standard          | Details (link) |
|-------|----------------------------------|-------------------|-------------------| :------: |
| 🆔    | Patient Identification Service   | In operation      | HL7 V3  | X |
| 🆔    | Patient Identification Service   |  Pre-production     | HL7 FHIR  | [Patient Identification Scenario](bus-scenario-pat.html) |

-----------------------------



<div>
<p> </p>
<p>The figure below provides a graphical representation of all these services</p>
{% include arch-bus-services.svg %}
<p>Figure 1 - MyHealth@EU Services</p>
<p> </p>
</div>

<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: by clicking on the business service icon you are redirected to use case descriptions in the 
            <a href="https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/1.+MyHealth@EU+Requirements+Catalogue" target="_blank">
                MyHealth@EU requirement catalogue
            </a>.
        </td>
    </tr>
</table>
</div>

-------------------------


### Common Business Processes

The business processes associated with these services can be generalized through a set of common (meta) business processes:

|       | Processes                        | Status            | Application View |
|-------|----------------------------------|-------------------| -------------------|
| 📝    | Patient Identification   | Pre-production       | [Patient Identification](application.html)   |  
| 💊    | Document Retrieval  | Pre-production       | [Document Retrieval](application.html)  | 
| 📄    | Document Provision       | Under definition      | [Document Provision](application.html)   |
| 💊    | Study Retrieval  | Under definition      | To be documented  | 
| 📄    | Images Retrieval       | Under definition     | To be documented     |


 which can be grouped into three main categories:

* Security context establishment
* Document exhange and handling
* Images Study exchange and handling


<div>
<p>as illustrated by the following figure:</p>
<p> </p>
{% include arch-bus-processes.svg %}
<p>Figure 1 - MyHealth@EU Common Business Processes</p>
</div>