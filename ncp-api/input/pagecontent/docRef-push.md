{% include fsh-link-references.md %}

There are different possible options, not mutually exclusive, that can be adopted for providing reports by using HL7 FHIR API; in this first version, this will be realized by **creating (updating) a DocumentReference**

This option infact allows to search for any kind of document indipendently by its format. For example it could include and/or refer the PDF and the corresponding FHIR (or CDA) representation.

A DocumentReference resource can refer/include more representations of the same document.

Bundle transaction or batch could be used to gather multimple DocumentReference POST and PUT operations in a single call.


{% include sequence-docRef-push.svg %}
