### Scope

This HL7 FHIR Implementation Guide describes the services used in MyHealth@EU, including the interface specifications used in the gateway (NCPeH) to gateway (NCPeH) communications.

### Introduction

**MyHealth@EU** is the **European digital health infrastructure** designed to support the secure **cross-border exchange of health data** among **EU Member States** through a set of services, including:  

- **Patient Summary**  
- **ePrescription / eDispensation**  
- **Original Clinical Document**  
- **Laboratory Results Report**  
- **Hospital Discharge Report**  
- **Medical Imaging Studies**  

The **maturity of these services**, as well as the **technology used to implement them**, varies depending on the **priority category** documented.  

The following figure provides an **overview of their maturity levels and the technologies used**.  


<div>
<p> </p>
<img src="services-overview.png" alt="Overview of MyHealth@EU Services" style="max-width: 70%; height: auto;">
<p>Figure 1 - Overview of MyHealth@EU Services</p>
<p> </p>
</div>


This version of this guide focuses on the new MyHealth@EU services based on the HL7 FHIR technology, with the goal to be ther one-stop-shop guide for all the specifications for the MyHealth@EU Node to Node communications.

It will define morevoer the capabilities of the HL7 FHIR API offered by the MyHealth@EU NCPeH for supporting:

* the identification of the patient
* the retrieval of relevant document metadata
* the retrieval of selected documents
* the provision of selected documemts
* the search and retrieval of imaging study information


**No liability can be however inferred from the use or misuse of this specification, or its consequences; for the services not released.**


### How to read this guide

Readers are encouraged to familiarize themselves with how information is organized in this guide.

The information in this guide is structured according to the BDAT model (Business, Data, Application, and Technology), as illustrated in the figure below.

  <div>
  <p> </p>
    {% include bdat.svg %}
    <p><caption>Fig. 1: Navigating this guide</caption></p>  
  </div>


Where:
* the **Business domain** describes what is supposed to be realized and why from a user perspective. It may include description of the Business goals, Use Cases, requirements, processes and rules. The main reference for this part is the [MyHealth@EU requirement catalogue](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/1.+MyHealth@EU+Requirements+Catalogue)
* The **Data (Information) domain** describes the content that is exchanged and it is documented in the domain specific content guides, based on the data set described in the [requirement catalogue](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/1.+MyHealth@EU+Requirements+Catalogue) 
* the **Application (Computational) domain**, describing how gateways interact in a implementation indipendent form, to fullfill the business requirements. It may include sequence diagrams.
* The **Technology (Engineering) domain**, describes the technology stack required to support the MyHealth@EU processes. This includes how HL7 FHIR, or other standards adopted, are used for implementing the documented services.