{% include fsh-link-references.md %}


### Business View

The overall business scenarios are described in the [Business view page](business.html)

### Application View

The implementation-indipendent sequence diagrams is described in the [Application view page](application.html#patient-identification)

### Implementation

The Patient Identfication is realized by performing these steps:
- The **Requesting NCP** sends a **Patient search request** to the **Responding NCP**.
- The **Responding NCP** searches for the requested patient(s) and returns a **HL7 FHIR Bundle** containing **Patient** resources.
- The returned **Bundle** contains all matching **Patient** entries, based on the provided search parameters.



<div>
<p> </p>
{% include sequence-pat.svg %}
<p>Figure 1 - Patient Identification</p>
<p> </p>
</div>



#### Step-by-Step Breakdown: Patient Search

##### eHDSI Patient Identification Request

The **Requesting NCP** initiates a search query for a **Patient** using the FHIR API:

  ```
  GET $server/Patient?<searchParameters><modifiers>
  ```

The query includes **search parameters** and **modifiers** to refine the search criteria.
Details below.

###### Search parameters

| Parameter | Opt | searchParameter | type | notes |
| ---- |---- | ----- | --- | --- |
| business identifier | SHOULD |  [identifier](https://hl7.org/fhir/R4/patient.html#search) | [token](https://hl7.org/fhir/R4/patient.html#:~:text=identifier%20TU-,token,-A%20patient%20identifier) |
| name | MAY | [name](https://hl7.org/fhir/R4/patient.html#search) | [string](https://hl7.org/fhir/R4/search.html#string)  |
| birth date | MAY | [birthdate](https://hl7.org/fhir/R4/patient.html#search) | [date](https://hl7.org/fhir/R4/search.html#date)  |
| gender | MAY | [gender](https://hl7.org/fhir/R4/patient.html#search) | [token](https://hl7.org/fhir/R4/patient.html#:~:text=identifier%20TU-,token,-A%20patient%20identifier)  |


**Search result parameters**

Consumers are suggested to explictly indicate the elements that are expected to be used for the Patient Identification and minimize the patient information exchanged, by using the [_elements](https://hl7.org/fhir/R4/search.html#elements) Search result parameter.

###### Modifiers

No [modifers](https://hl7.org/fhir/R4/search.html#modifiers) required


##### eHDSI Patient Identification Response

The **Responding NCP** processes the request and returns a **FHIR Bundle** containing the search results, in accordance with the rules specified in [HL7 FHIR - RESTful Search)](https://hl7.org/fhir/R4/http.html#search) and in [Search Results](https://hl7.org/fhir/R4/bundle.html#searchset).

The Bundle contains entries of type **Patient**:

  ```
  Bundle.where(type='searchSet').entry.resource.ofType(Patient)
  ```

The Bundle of type 'searchSet' should be created 

**The National Infrastructure returns Patient resources conformant with the [PatientMinimalMyHealthEu] profile.**

| Parameter | Opt | 
| ---- |---- | 
| business identifier | SHALL |
| name | SHALL | 
| birth date | SHALL | 
| gender | SHALL | 


#### HTTP Error Response Codes

| HTTP Response | Description |
| -- | -- |
| 401 Unauthorized | authorization is required for the interaction that was attempted |
| 404 Not Found | resource type not supported, or not a FHIR end-point |
| 410 Gone | resource deleted or no more active |


#### Examples

All patients with this identifier

> `GET $server/Patient?identifier=http://identifiers.mycountry|123456789`

All patients with this identifier (only essential information returned)

> `GET $server/Patient?identifier=http://identifiers.mycountry|123456789&_elements=identifier,active,name,birthDate,gender`

All patients with both identifiers

> `GET $server/Patient?identifier=http://first-id.mycountry|123456789&http://second-id.mycountry|987654321`

All patients with in the name "doe" born the 1985-01-14 and of male gender

> `GET $server/Patient?name=doe&birthdate=eq1985-01-14&sex=male`


