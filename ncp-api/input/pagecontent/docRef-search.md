{% include fsh-link-references.md %}

There are different possible options, not mutually exclusive, that can be adopted for searching/retrieving or fetching reports by using HL7 FHIR API, see for details the [Report Search Page](sequence-docs.html)

In this first version, this will be realized by **searching per DocumentReference**

This option infact allows to search for any kind of document indipendently by its format. For example it could include and/or refer the PDF and the corresponding FHIR (or CDA) representation.

A DocumentReference resource can refer/include more representations of the same document.


{% include sequence-docRef.svg %}


#### Request: Search / Fetch Report(s)

| Parameter | Consumer | Provider | searchParameter | type | notes |
| ---- |---- | ----- | --- |--- | ---|
| Document type | SHALL | SHALL |  [type](https://hl7.org/fhir/R4/documentreference.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) |
| Type of content | MAY | SHALL | [contenttype](https://hl7.org/fhir/R4/documentreference.html#search) | [token](https://hl7.org/fhir/R4/search.html#token) | |
| Study type | MAY | SHALL | [category](https://hl7.org/fhir/R4/documentreference.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) |
| Patient business identifier | SHALL | SHALL | [patient](https://hl7.org/fhir/R4/documentreference.html#search) | [reference](https://hl7.org/fhir/R4/search.html#reference)  | It requires chained parameter  (patient.identifier) supported |
| Date | SHALL | SHALL | [date](https://hl7.org/fhir/R4/documentreference.html#search) | [date](https://hl7.org/fhir/R4/search.html#date)  | |
| Creation Date | MAY | MAY | [creation](https://profiles.ihe.net/ITI/MHD/SearchParameter-DocumentReference-Creation.html) | [date](https://hl7.org/fhir/R4/search.html#date)  | It specifies a search against the DocumentReference.content.attachment.creation |

**The provider shall support the combination of required search parameters**

Please note that the *date* parameter refers to the date when the DocumentReference has been created. A query against the effective document creation date may be optionally perfomed by using the SearchParameter [creation](https://profiles.ihe.net/ITI/MHD/SearchParameter-DocumentReference-Creation.html).


##### Examples

All the lab reports of the Czech patient 456789123 created from 2023, Feb 20th.

> `GET $server/DocumentReference?type=http://loinc.org|11502-2&patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&date=ge2023-02-20`

All the lab reports of the Czech patient 456789123 of type PDF.

> `GET $server/DocumentReference?type=http://loinc.org|11502-2&patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&contenttype=application/pdf`

All the Microbiology lab reports of the Czech patient 456789123 of the 6 months (assuming that today is 2024-03-25)

> `GET $server/DocumentReference?type=http://loinc.org|11502-2&patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&category=http://loinc.org|18725-2&date=ge2023-09-25

#### Response: Search / Fetch Report(s)

If present, a search set Bundle including a set of DocumentReference compliant with the [DocumentReferenceMyHealthEu] profile is returned.

Each DocumentReference may return or refer one or more representations (e.g. PDF, HL7 CDA, HL7 FHIR json) of a specific document instance.

If the document is included in the content.data element we are in the FETCH case.

If the document is referred in the content.url element we are in the SEARCH AND GET case.

In this case it is expected that the [*url*](https://hl7.org/fhir/R4/datatypes-definitions.html#Attachment.url) element contains the actual url to be used for retrieving this docuemnt from the HL7 FHIR API.

#### Request: Getting Report(s) by ID

In the SEARCH AND GET case for each selected document it is possible to retrieve the document(s) by getting them using the provided url.

Two options are considered:
1. the content type is HL7 FHIR (it might be json, xml or RDF)
1. the content type is not HL7 FHIR (e.d. PDF, HL7 CDA)

##### HL7 FHIR documents

In the first case (HL7 FHIR documents) the resource to be retrieved is of type [Bundle](https://hl7.org/fhir/R4/bundle.html) and the Request will be of type:

> `GET $server/Bundle/id`

##### NON HL7 FHIR documents

In the second case (non HL7 FHIR documents) the resource to be retrieved is of type [Binary](https://hl7.org/fhir/R4/binary.html) and the Request will be of type:

> `GET $server/Binary/id`

#### Response: Getting Report(s) by ID

##### HL7 FHIR documents

A Document Bundle conformant with the profile defined for that kind of document is returned.

##### NON HL7 FHIR documents

A Document Binary including the document in PDF or HL7 CDA is returned.

#### HTTP Error Response Codes

| HTTP Response | Description |
| -- | -- |
| 401 Unauthorized | authorization is required for the interaction that was attempted |
| 404 Not Found | resource type not supported, or not a FHIR end-point |
| 410 Gone | resource deleted or no more active |