### Data Domain


The [**data domain**](content.html) describes data models and exchange formats used by MyHealth@EU.

{% include bdat-d.svg %}


Reference data sets are documented in the [MyHealth@EU requirement catalogue](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/1.+MyHealth@EU+Requirements+Catalogue).


<div>
<p> </p>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: A representation of these data sets as HL7 FHIR Logical Model is provided in the Laboratory and Hospital Discharge Report HL7 FHIR IGs.
        </td>
    </tr>
</table>
<p> </p>
</div>



|  | Data Sets  | Specifications |
|------|-------------|-------------|
| <img src="medical-app.png" alt="Patient Summary icon" style="width:30px; vertical-align:middle;"> | [Patient Summary Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/05.01.+Create+the+MyHealth@EU+Patient+Summary+content#id-05.01.CreatetheMyHealth@EUPatientSummarycontent-Dataelementsdescription) | [eHDSI Patient Summary - HL7 CDA R2](https://art-decor.ehdsi.eu/publication/)
|------|-------------|-------------|
| <img src="prescription.png" alt="Medicine Prescription and Dispense icon" style="width:30px; vertical-align:middle;"> | [ePrescription Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/06.01.+Create+the+MyHealth@EU+ePrescription%28s%29+content#id-06.01.CreatetheMyHealth@EUePrescription(s)content-Dataelementsdescription) | [ePrescription - HL7 CDA R2](https://art-decor.ehdsi.eu/publication/)
|------|-------------|-------------|
| <img src="prescription.png" alt="Medicine Prescription and Dispense icon" style="width:30px; vertical-align:middle;"> | [eDispensation Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/07.01.+Create+the+MyHealth@EU+eDispensation+content#id-07.01.CreatetheMyHealth@EUeDispensationcontent) | [eDispensation - HL7 CDA R2](https://art-decor.ehdsi.eu/publication/)
|------|-------------|-------------|
| <img src="experiment-results.png" alt="MyHealth@EU Laboratory Report icon" style="width:30px; vertical-align:middle;"> | [Laboratory Results Report](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/13.01+Create+the+MyHealth@EU+Laboratory+Result+Report+Content) | [Laboratory Report - HL7 FHIR](https://fhir.ehdsi.eu/laboratory)
|------|-------------|-------------|
| <img src="hospital.png" alt="Hospital Discharge Report icon" style="width:30px; vertical-align:middle;"> | [Hospital Discharge Report Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/14.01+Create+the+MyHealth@EU+Hospital+Discharge+Report+content) | [Hospital Discharge Report - HL7 FHIR](https://fhir.ehdsi.eu/build/hdr/)
|------|-------------|-------------|
| <img src="radiology.png" alt="Digital Imaging Report icon" style="width:30px; vertical-align:middle;"> | [Medical Imaging Report](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/15.01+Create+the+MyHealth@EU+Medical+Imaging+Studies+and+Report+content) | Medical Imaging Report - HL7 FHIR
|------|-------------|-------------|
| <img src="hospital.png" alt="Hospital Discharge Report icon" style="width:30px; vertical-align:middle;"> | [Original Clinical Document Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/08.01.+Create+the+MyHealth@EU+Original+Clinical+Document+content) | [Original Clinical Document - HL7 CDA R2](https://art-decor.ehdsi.eu/publication/)
|------|-------------|-------------|
