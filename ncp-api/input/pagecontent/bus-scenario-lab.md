{% include fsh-link-references.md %}

This page describes the business scenarios for the **Laboratory Result Report Use Case**.

This scenario can be realized by using a common interaction pattern describing the search and retrieval of a document.

Future specifications may differentiate the cases based on the search paramters used and the kind of data that is supposed to be seaerched and retrieved.

### Scenario

#### Laboratory Result Report sharing on a cross-border scale

1. Check Patient ID
1. Once the identity of the patient is validated, the HP of country B queries country A for a list of laboratory result reports of that patient, based on the 1. query parameters
1. The HP interface requests the lists of Laboratory Results Report of country A to the NCP of country B. 
1. The NCP of country B requests the list of LRR to the NCP of country A
1. The NCP of country A, after checking if patient consent has been provided (where applicable), gets and provides to the NCP of country B the list of Laboratory 1. Results Report of country A available for the patient matching query parameters.
1. The NCP of country B conveys the list to HP interface.
1. The HP-B selects all or subset of reports and requests them to the NCP of country B.
1. The NCP of country B request the selected reports to the NCP of country A.
1. The NCP of country A  gets and provides to the NCP of country B the requested LRR of country A in the MyHealth@EU format.
1. The NCP of country B conveys the LRR of country A to HP interface.
1. The HP interface provides a single laboratory result report view.
1. The HP-B accesses to the Patient's Laboratory Result Report of country A.
1. The use case is terminated.


### Diagram 

#### Laboratory Result Report sharing on a cross-border scale

{% include sequence-business-getDocs.svg %}


<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: click on the diagram to see more details about each interaction 
            <a href="application.html" target="_blank">
                [application view]
            </a>.
        </td>
    </tr>
</table>
</div>

