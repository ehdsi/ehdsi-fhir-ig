{% include fsh-link-references.md %}

The HL7 FHIR [ImagingStudy](https://hl7.org/fhir/R4/imagingstudy.html) resource is a *representation of the content produced in a DICOM imaging study.* and it 
represents only one DICOM Study an it may reference the whole study or *a subset of that Study. More than one ImagingStudy MAY reference the same DICOM Study or different subsets of the same DICOM Study.*.


{% include sequence-fhir-study.svg %}

#### Request: Search / Fetch Imaging Study(s)

| Parameter | Consumer | Provider | searchParameter | type | notes |
| ---- |---- | ----- | --- |--- | ---|
| Body site | SHOULD | SHOULD |  [bodysite](https://hl7.org/fhir/R4/imagingstudy.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) | The body site studied |
| Instance SOP Class | MAY | MAY |  [dicom-class](https://hl7.org/fhir/R4/imagingstudy.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) | The type of the instance (SOP Class) |
| Identifier | SHALL | SHALL |  [identifier](https://hl7.org/fhir/R4/imagingstudy.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) | Identifiers for the Study |
| Modality | SHALL | SHALL |  [modality](https://hl7.org/fhir/R4/imagingstudy.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) | The modality of the series |
| Patient business identifier | SHALL | SHALL | [patient](https://hl7.org/fhir/R4/imagingstudy.html#search) | [reference](https://hl7.org/fhir/R4/search.html#reference)  | It requires chained parameter  (patient.identifier) supported |
| Study reason | MAY | SHALL | [reason](https://hl7.org/fhir/R4/imagingstudy.html#search) | [token](https://hl7.org/fhir/R4/search.html#token) | The reason for the study |
| Study Date | SHALL | SHALL | [started](https://hl7.org/fhir/R4/imagingstudy.html#search) | [date](https://hl7.org/fhir/R4/search.html#date)  | When the study was started |


**The provider shall support the combination of required search parameters**



##### Examples

All the Imaging Studies of the Czech patient 456789123 started on 2024.

> `GET $server/ImagingStudy?patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&started=ge2024-01-01&started=le2024-12-31`


All the Imaging Studies of the Czech patient 456789123 made in reason of a Kidney disease (disorder) (SCT 90708001)

> `GET $server/ImagingStudy?patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&reasonCode=http://snomed.info/sct|90708001`

All the Imaging Studies of the Czech patient 456789123 including Magnetic Resonance series

> `GET $server/ImagingStudy?patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&modality=MR`

#### Response: Search / Fetch Report(s)

If present, a search set Bundle including a set of ImagingStudy compliant with the [ImagingStudy] (*to be specified*) profile is returned.

#### HTTP Error Response Codes

| HTTP Response | Description |
| -- | -- |
| 401 Unauthorized | authorization is required for the interaction that was attempted |
| 404 Not Found | resource type not supported, or not a FHIR end-point |
| 410 Gone | resource deleted or no more active |