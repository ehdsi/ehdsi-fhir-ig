{% include fsh-link-references.md %}

This page describes the main business scenarios covered by this guide:
* Consult and Obtain Laboratory Results cross-borders
* Consult and Obtain Imaging Reports cross-borders
* Consult and Obtain Hospital Discharge Reports cross-borders
* Request and retrieval of imaging studies and imaging reports cross-borders
* Send Hospital Discharge Reports cross-borders


At this stage the first three cases have been represented by using a generic common interaction describing a retrieval of a document. Future specifications may differentiate these cases based on the search paramter and the kind of datat that is supposed to be seaerched and retrieved.


### Consult and Obtain Laboratory Results, Imaging Reports and Hospital Discharge Reports

**Note: limited to Laboratory Results in wave 8**

{% include sequence-business-getDocs.svg %}

###  Request and retrieval of imaging studies and imaging reports

**Note: not for wave 8, under definition for wave 9**

This case is described in the [eHN Network guidelines](https://health.ec.europa.eu/document/download/0079ad26-8f8f-435b-9472-3cd8625f4220_en?filename=ehn_mi_guidelines_en.pdf) as Use Case 1, with associated this Functional Process:
1. The Patient from Country A consults a Health Professional in the country of treatment (Country B).
2. The Health Professional (in Country B) is identified, authenticated and authorised.
3. The Health Professional (in Country B) provides information to the patient on how personal health data in the imaging studies and reports will be collected and processed in Country B.
4. The Patient is identified (in Country B) and the patient identity is confirmed by the country of affiliation (Country A).
5. The Health Professional in Country B queries Country A for a list of imaging studies and imaging reports of that patient, providing, if needed, additional query parameters, such as time period, modality, study procedure, body part, or procedure code.
6. Country A provides a list of the available imaging studies and/or reports to Country B.
7. The Health Professional selects (including filtering, ordering, grouping) and requests the relevant imaging studies and/or reports from Country A.
8. Country A provides the requested imaging studies and/or reports to Country B. In case there are multiple versions of studies/reports, the latest versions are provided.
9. The Health Professional (in Country B) is presented with the requested imaging studies and/or reports through a user interface provided by a local information system, a dedicated viewer, a portal or an alternative technical solution. The Health Professional may also be enabled to download the requested imaging studies and/or reports to the system used locally for later use if allowed by the national law and relevant data  protection arrangements.
10. The Health Professional (in Country B) uses the imaging studies and/or reports to provide healthcare service


* Steps 1-4 are covered by the "Patient Search" fragment.
* Step 5-6 by the Search and retrieve study infos and reports fragment.
* Steps 7 by the Get and Select Study/Series/Instance ids fragment
* Step 8-10 by te Get Images fragment


{% include sequence-business-imgStudy.svg %}

###  Send Hospital Discharge Reports

**Note: not for wave 8, under definition for wave 9**

{% include sequence-business-sendDocs.svg %}
