{% include fsh-link-references.md %}

This page describes the sequence fragments used by the Consult and Obtain Laboratory Results, Imaging Reports and Hospital Discharge Reports;referring the possible implementation options.


#### Patient search

The Patient search is common to all the options.

{% include sequence-pat.svg %}

