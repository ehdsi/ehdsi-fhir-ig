{% include fsh-link-references.md %}

### Other options

There are different possible options - some of them listed below -  for searching and retrieving reports, or other documents, by using the FHIR API.

1. [searching per DocumentReference](docRef-search.html)
1. [searching per Composition](#composition-search)
1. [searching per Bundle](#bundle-search)
1. [searching per DiagnosticReport](#diagnosticreport-search) 
 

They are not mutually exclusive, that is, depending on the goal one or the other could be chosen. 
However, for this first implementation only the DocumentReference will be used and specified; the other are described only for documentation purposes.


#### Search and Get Report(s)


#### Composition Search

{% include sequence-comp.svg %}

Search parameters:
* Document type
* Study type
* Specialty
* Patient business identifier
* period of time

#### Bundle Search

{% include sequence-bundle.svg %}

#### DiagnosticReport Search

{% include sequence-dr.svg %}

Search parameters:
* Document type
* Study type
* Specialty
* Patient business identifier
* period of time