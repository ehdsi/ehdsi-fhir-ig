{% include fsh-link-references.md %}

### MyHealth@EU Study Information Retrieval service
- **DICOM Version Supported**: DICOM 2020e

---

### Introduction

The MyHealth@EU Study Information Retrieval service is a DICOM-compliant service that provides support for DICOMweb-based RESTful services, including QIDO-RS and WADO-RS. This conformance statement outlines the supported DICOM SOP classes, Transfer Syntaxes, and Query/Retrieve capabilities.

---

### Supported DICOM SOP Classes

| **SOP Class Name**                            | **SOP Class UID**           | **Service Type**        | **Role** |
|-----------------------------------------------|-----------------------------|-------------------------|----------|
| Query/Retrieve Information Model - FIND (Study) | `1.2.840.10008.5.1.4.1.2.2.1` | QIDO-RS                  | SCP      |
| Query/Retrieve Information Model - FIND (Series) | `1.2.840.10008.5.1.4.1.2.2.1` | QIDO-RS                  | SCP      |
| Web Access to DICOM Objects (WADO-RS)         | `1.2.840.10008.5.1.4.1.2.5.3` | WADO-RS                  | SCP      |

- **SCP**: Service Class Provider – The system acts as a provider for queries and retrievals.

---

### DICOMweb Services Supported

#### QIDO-RS (Query based on ID for DICOM Objects - RESTful Service)

**Description**: Provides querying of DICOM studies, series, and instances via RESTful APIs using standard DICOM Query/Retrieve SOP Classes.

- **Base URL**: `/dicom-web/studies`
- **HTTP Methods**: `GET`
- **Supported Query Parameters**:
  - `PatientID`: Query based on Patient ID
  - `StudyInstanceUID`: Query based on Study Instance UID
  - `Modality`: Filter based on modality (e.g., `MR`, `CT`, `XR`)
  - `StudyDate`: Filter based on study date
  - `SeriesInstanceUID`: Query specific series within a study

##### Example Request:

```
GET /dicom-web/studies?PatientID=123456789&Modality=MR
```

##### Example Response (JSON Format):

```json
[
  {
    "0020000D": { "vr": "UI", "Value": ["1.2.840.113619.2.55.3.2831164356.1234.1578912545.100"] },
    "00080020": { "vr": "DA", "Value": ["20220101"] },
    "00080061": { "vr": "CS", "Value": ["MR"] },
    "00100010": { "vr": "PN", "Value": [{"Alphabetic": "Doe^John"}] },
    "00100020": { "vr": "LO", "Value": ["123456789"] },
    "00081030": { "vr": "LO", "Value": ["Brain MRI with Contrast"] }
  }
]
```

#### WADO-RS (Web Access to DICOM Objects - RESTful Service)

**Description**: Provides retrieval of DICOM objects (e.g., studies, series, instances) via RESTful APIs. The system supports retrieval of images, metadata, and bulk pixel data using HTTP methods.

- **Base URL**: `/dicom-web/studies/{StudyInstanceUID}/series/{SeriesInstanceUID}/instances`
- **HTTP Methods**: `GET`
- **Supported Transfer Syntaxes**:
  - Explicit VR Little Endian (1.2.840.10008.1.2.1)
  - JPEG Baseline (1.2.840.10008.1.2.4.50)
  - JPEG-LS Lossless (1.2.840.10008.1.2.4.80)
  - MPEG-2 (1.2.840.10008.1.2.4.100)

##### Example Request:

```
GET /dicom-web/studies/1.2.840.113619.2.55.3.2831164356.1234.1578912545.100/series/1.2.840.113619.2.55.3.2831164356.1234.1578912545.200/instances
```

##### Example Response (Bulk Data):
- Response format: Multipart or DICOM Binary
- Contains the requested series or instances with pixel data.

---

### Security

- **Authentication**: OAuth 2.0
- **Data Encryption**: TLS 1.2 or higher
- **Access Control**: Role-based access control (RBAC) for data access and query permissions.

---

### Network Communication Protocol

- **Protocol**: HTTP/1.1 or HTTPS
- **Port**: 80 (HTTP), 443 (HTTPS)
- **Data Format**: JSON for query responses, DICOM Binary for object retrieval

---

### Error Handling

The system supports the following HTTP response codes:

- **200 OK**: Request was successful, and the response contains the requested information.
- **400 Bad Request**: Invalid parameters in the request.
- **404 Not Found**: Requested resource (e.g., study, series) could not be found.
- **500 Internal Server Error**: A server-side error occurred.

---

### Compliance with DICOM Standards

- **DICOM Standard Version**: 2020e
- **RESTful Web Services Standard**: DICOM PS3.18
- **Data Encapsulation**: Support for JSON and XML representations of DICOM metadata.


#### 8Examples

##### Query for a Study for this patient

###### Request

Give me the studies of this Patient

```
`GET $DICOMWeb-server/studies?PatientID=123456789
```

###### Response

```json
[
  {
    "0020000D": {
      "vr": "UI",
      "Value": [
        "1.2.840.113619.2.55.3.2831164356.1234.1578912545.100"
      ]
    },
    "00080020": {
      "vr": "DA",
      "Value": [
        "20220101"
      ]
    },
    "00080030": {
      "vr": "TM",
      "Value": [
        "090000"
      ]
    },
    "00080061": {
      "vr": "CS",
      "Value": [
        "MR"
      ]
    },
    "00100010": {
      "vr": "PN",
      "Value": [
        {
          "Alphabetic": "Doe^John"
        }
      ]
    },
    "00100020": {
      "vr": "LO",
      "Value": [
        "123456789"
      ]
    },
    "00200010": {
      "vr": "SH",
      "Value": [
        "STUDY12345"
      ]
    },
    "00081030": {
      "vr": "LO",
      "Value": [
        "Brain MRI with Contrast"
      ]
    }
  }
]
```

####### Explanation of Response Fields:

- **`0020000D` (Study Instance UID)**:
  - `vr: "UI"`: Value Representation of UID (Unique Identifier).
  - The `Value` array contains the Study Instance UID, which uniquely identifies the study.
  
- **`00080020` (Study Date)**:
  - `vr: "DA"`: Value Representation of Date.
  - The `Value` array contains the study date (`20220101` represents January 1, 2022).

- **`00080030` (Study Time)**:
  - `vr: "TM"`: Value Representation of Time.
  - The `Value` array contains the study time (`090000` represents 09:00 AM).

- **`00080061` (Modalities in Study)**:
  - `vr: "CS"`: Value Representation of Code String.
  - The `Value` array contains the modalities used in the study (e.g., "MR" for MRI).

- **`00100010` (Patient's Name)**:
  - `vr: "PN"`: Value Representation of Person Name.
  - The `Value` array contains the patient's name (`"Doe^John"` means the patient's name is John Doe).

- **`00100020` (Patient ID)**:
  - `vr: "LO"`: Value Representation of Long String.
  - The `Value` array contains the Patient ID (`"123456789"`).

- **`00200010` (Study ID)**:
  - `vr: "SH"`: Value Representation of Short String.
  - The `Value` array contains the Study ID (`"STUDY12345"`).

- **`00081030` (Study Description)**:
  - `vr: "LO"`: Value Representation of Long String.
  - The `Value` array contains the description of the study (`"Brain MRI with Contrast"`).

**DICOM Tags Used:**

- **`0020000D`**: Study Instance UID
- **`00080020`**: Study Date
- **`00080030`**: Study Time
- **`00080061`**: Modalities in Study
- **`00100010`**: Patient's Name
- **`00100020`**: Patient ID
- **`00200010`**: Study ID
- **`00081030`**: Study Description

This JSON response provides essential metadata about the study but does not include the bulk image data (which can be retrieved via different endpoints like `WADO-RS` for fetching image instances).