{% include fsh-link-references.md %}

This page describes the sequence fragments used by the Consult and Obtain Laboratory Results, Imaging Reports and Hospital Discharge Reports, referring the possible implementation options.


### Business View

The overall business scenarios are described in the [Business view page](business.html)

### Application View

The implementation-indipendent sequence diagrams is described in the [Application view page](application.html#patient-identification)


### Implementation alternatives

There are multiple alternatives, not mutually exclusive, for searching, retrieving, or fetching documents using the HL7 FHIR API.

The selected mechanism may depend on the type of document and the usage context.

For example, searching for documents via **DocumentReference** - the query type used in the IHE MHD profile - enables a standardized retrieval mechanism regardless of document format. This approach is suitable for simple search mechanisms and requires that a DocumentReference instance is generated for each created document.

On the other hand, searching via **DiagnosticReport** allows for more advanced queries, but it is limited to imaging and diagnostic reports, as well as HL7 FHIR documents. Other mechanisms may apply in different contexts.

In this first version, this will be realized by **searching per DocumentReference**

Some alternatives - described only for documentation purposes - are listed below:
1. [searching per Composition](otherOptions.html)
1. [searching per Bundle](otherOptions.html)
1. [searching per DiagnosticReport](otherOptions.html) (only for imaging and laboratory reports)


### Implementation

Beside the advatages described above, the DocumentReference Search mechanism permits to realize with a common call both the *search and retrieve* and the *document fetch* envisioned by the [Document Retrieval](application.html#document-retrieval) sequence.


<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: When used for realizing the search and retrieve feature it complies with the IHE MHD profile.</td>
    </tr>
</table>
</div>

In the following diagram is described how the requesting NCP realizes the search or the fetch of the document by using the DocumentReference.

In summary:
* The Requesting NCP searches for a DocumentReference.
* If content data exists, the document is fetched directly.
* If content data is missing, the system retrieves it via a HL7 FHIR Bundle or Binary resource.



<div>
<p> </p>
{% include sequence-docRef.svg %}
<p>Figure 1 - Patient Identification</p>
<p> </p>
</div>


#### Step-by-Step Breakdown: DocumentReference Search

##### eHDSI DocumentReference Search Request

 The **Requesting NCP** initiates a search query for a **DocumentReference** using the FHIR API:

  ```
  GET $server/DocumentReference?<searchParameters><modifiers>
  ```

The query includes **search parameters** and **modifiers** to refine the search criteria.

##### Search Parameters

| Parameter | Consumer | Provider | searchParameter | type | notes |
| ---- |---- | ----- | --- |--- | ---|
| Document type | SHALL | SHALL |  [type](https://hl7.org/fhir/R4/documentreference.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) |
| Type of content | MAY | SHALL | [contenttype](https://hl7.org/fhir/R4/documentreference.html#search) | [token](https://hl7.org/fhir/R4/search.html#token) | |
| Study type | MAY | SHALL | [category](https://hl7.org/fhir/R4/documentreference.html#search)  | [token](https://hl7.org/fhir/R4/search.html#token) |
| Patient business identifier | SHALL | SHALL | [patient](https://hl7.org/fhir/R4/documentreference.html#search) | [reference](https://hl7.org/fhir/R4/search.html#reference)  | It requires chained parameter  (patient.identifier) supported |
| Date | SHALL | SHALL | [date](https://hl7.org/fhir/R4/documentreference.html#search) | [date](https://hl7.org/fhir/R4/search.html#date)  | |
| Creation Date | MAY | MAY | [creation](https://profiles.ihe.net/ITI/MHD/SearchParameter-DocumentReference-Creation.html) | [date](https://hl7.org/fhir/R4/search.html#date)  | It specifies a search against the DocumentReference.content.attachment.creation |

**The provider shall support the combination of required search parameters**

Please note that the *date* parameter refers to the date when the DocumentReference has been created. 
A query against the effective document creation date may be optionally perfomed by using the SearchParameter [creation](https://profiles.ihe.net/ITI/MHD/SearchParameter-DocumentReference-Creation.html).

##### eHDSI DocumentReference Search Response

 The **Responding NCP** returns a **FHIR Bundle** containing the search results. 
 The bundle contains entries of type **DocumentReference**, compliant with the [DocumentReference: Minimal](StructureDefinition-DocumentReference-minimal-myhealtheu.html)

  ```
  Bundle.where(type='searchSet').entry.resource.ofType(DocumentReference)
  ```

  
Each DocumentReference may return or refer one or more representations (e.g. PDF, HL7 CDA, HL7 FHIR json) of a specific document instance.

If the document is included in the content.data element we are in the FETCH case.

If the document is referred in the content.url element we are in the SEARCH AND GET case.

In this case it is expected that the [*url*](https://hl7.org/fhir/R4/datatypes-definitions.html#Attachment.url) element contains the actual url to be used for retrieving this docuemnt from the HL7 FHIR API.


##### Checking DocumentReference Content Availability

If **DocumentReference.content.data.exists()**, the document can be **fetched directly** from the `DocumentReference.content.data` field and the sequence ends.

Otherwise,

##### Retrieving the Document by URL

if **DocumentReference.content.data is empty**, the system must **search and retrieve** the document from the specified URL.


Two options are here considered:
1. the content type is HL7 FHIR (it might be json, xml or RDF) (e.g. #application/fhir+json)
1. the content type is not HL7 FHIR (e.d. PDF, HL7 CDA)


##### eHDSI Get Bundle by Id Request

 If the **content type** of the document is `application/fhir+json` or `application/fhir+xml` or `application/fhir+rdf`, the system retrieves it using a **FHIR Bundle request**:

  ```
  GET $server/Bundle/id
  ```

  The **Requesting NCP** sends a request to the **Responding NCP** for the [Bundle](https://hl7.org/fhir/R4/bundle.html).
  The **Responding NCP** returns the [Bundle](https://hl7.org/fhir/R4/bundle.html), compliant with one of the porifles specified in the HL7 FHIR IG listed in the *Data* menu.


##### eHDSI Get Binary by Id Request

 Otherwise, if the **content type is not HL7 FHIR** (i.e., not `application/fhir+json`), the system retrieves it as a **HL7 FHIR Binary resource**:

  ```
  GET $server/Binary/id
  ```

  The **Requesting NCP** sends a request to fetch the document as a [Binary](https://hl7.org/fhir/R4/binary.html) resource.
  The **Responding NCP** returns the [Binary](https://hl7.org/fhir/R4/binary.html)  content.






#### HTTP Error Response Codes

| HTTP Response | Description |
| -- | -- |
| 401 Unauthorized | authorization is required for the interaction that was attempted |
| 404 Not Found | resource type not supported, or not a FHIR end-point |
| 410 Gone | resource deleted or no more active |

#### Examples

All the lab reports of the Czech patient 456789123 created from 2023, Feb 20th.

> `GET $server/DocumentReference?type=http://loinc.org|11502-2&patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&date=ge2023-02-20`

All the lab reports of the Czech patient 456789123 of type PDF.

> `GET $server/DocumentReference?type=http://loinc.org|11502-2&patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&contenttype=application/pdf`

All the Microbiology lab reports of the Czech patient 456789123 of the 6 months (assuming that today is 2024-03-25)

> `GET $server/DocumentReference?type=http://loinc.org|11502-2&patient.identifier=https://ncez.mzcr.cz/standards/fhir/sid/rid|456789123&category=http://loinc.org|18725-2&date=ge2023-09-25


