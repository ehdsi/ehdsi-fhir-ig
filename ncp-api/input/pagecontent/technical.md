
The **technology** domain describes the technology stack required to support the MyHealth@Eu services, this may include technical standards, specifications, and security considerations.


<div style="text-align: center">
{% include bdat-t.svg %}
</div>



### Overview

This section of the guide specifies how the **common business processes**, described in the [MyHealth@EU common business processes](business.html#common-business-processes), are implemented.  

For each **identified implementation-independent sequence diagram**, which outlines how the involved participants are expected to interact (as documented in the [application view](application.html#the-sequence-diagrams)), this guide provides **associated technical details** on how the implementation is carried out.  

Below is a summary of the available specifications for each **interaction scenario**:  

| Icon  | Interaction Scenarios                                    | Implementation Status   | Technical Specifications (link) |
|-------|------------------------------------------------------|----------------------|--------------------------------|
| 🆔    | [Patient Identification](application.html#patient-identification)  | Pre-production      | [Patient Identification](sequence-pat.html) |
| 📑    | [Document Retrieval](application.html#document-retrieval)          | Pre-production      | [Document Retrieval](sequence-docs.html) |
| 📤    | [Document Provision](application.html#document-provision)          | Under definition    | |
| 🖼️    | Study Retrieval                                       | Under definition    | To be documented |
| 🏥    | Images Retrieval                                      | Under definition    | To be documented |

