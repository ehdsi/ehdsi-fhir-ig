{% include fsh-link-references.md %}

This page describes the sequence fragments used by the Request and retrieval of imaging studies and imaging reports, referring the possible implementation options.

The overall business scenarios are described in the [Scenarios page](scenarios.html)

The Patient Search fragment is documented in the [Patient Search page](sequence-pat.html)

The Document Search fragments are documented in the [Document Search page](sequence-docs.html)



### Request and retrieve imaging studies

#### Business View

{% include sequence-business-imgStudy-details.svg %}


#### Application View

There are different implementation options, not mutually exclusive, for searching and retrieving Imaging Study information:

1. [searching per HL7 FHIR ImagingStudy](get-fhir-study.html)
1. [searching per DICOM Study metadata](get-dcm-study.html)

Implementation details are given in the linked pages.

It is not listed here the case of a document including study information, since this case falls into the [Search and Get Documents](sequence-docs.html) case.


##### Searching per HL7 FHIR ImagingStudy

In the first case Imaging Study information are shared by using an HL7 FHIR [ImagingStudy](https://hl7.org/fhir/R4/imagingstudy.html) resource.

Accoridng to the HL7 FHIR standard infact an ImagingStudy is a "*Representation of the content produced in a DICOM imaging study.* <...> *An ImagingStudy SHALL reference one DICOM Study, and MAY reference a subset of that Study. More than one ImagingStudy MAY reference the same DICOM Study or different subsets of the same DICOM Study.* ".

Specifications in the [Get Study Metadata (HL7 FHIR) page](get-fhir-study.html)

##### Searching per DICOM Study

In the second case a DICOMWeb call it is used to query the NCPeH.
A DICOM Conformance Statement describing how to realize this in MyHealth@EU is given in the [Study Information Retrieval page](get-dcm-study.html).

In IHE terms this refers to the  Retrieve [RAD-107] transaction (see [Section 4.107 IHE RAD TF Vol 2](https://www.ihe.net/uploadedFiles/Documents/Radiology/IHE_RAD_TF_Vol2.pdf).

In both cases the NCPeH API acts as a "facade" over the National Infrastructure, how studies are made available for cross border sharing depends on the national infrastructure.

Specifications in the [Get Study Metadata (DICOMweb) page](get-dcm-study.html)

### Get and Select Study/Series/Instance ids

#### Business View

{% include sequence-business-getImgRefs.svg %}

The user gets from the document and/or the study infos all the identifiers of the evidences belonging to the study or referred by the document.

Optionally he/she selects the level from which retrieve the instances (study, series, instance). That is all the instacne belonging to this study/seris or this list of instances.

### Get Images

#### Business View

{% include sequence-business-getImg-fragment.svg %}


#### Application View

Once known the relevant DICOM identifiers DICOM WADO-RS is used to retrieve
* All instances of a Study (getImages-sequence.html)
* All instances of a Series (getImages-sequence.html)
* All listed instances  (getImages-sequence.html)

A DICOM Confrmance Statement describing how to realize this in MyHealth@EU is given in the [Get Images page](get-instances.html)

