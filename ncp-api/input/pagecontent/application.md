{% include fsh-link-references.md %}

The **application domain** describes what are the logical (software) components involved in the [MyHealth@EU business services](business.html#business-services) and how they interact, collaborate or are coordinated. 

<div>
<p> </p>
{% include bdat-a.svg %}
<p> </p>
</div>


The business processes associated with these services can be generalized through a set of common (meta) business processes, as described in the [MyHealth@EU common business processes](business.html#common-business-processes).

In this version of the guide, sequence diagrams are used to illustrate how these common business processes are realized indipedently from their technical implementation.


### The MyHealth@EU Participants

The sequence diagrams documented in this guide identify a set of common participants: 

* The *Requestor* : the human actor managing the process.
* The *Requesting NCP* : the National Contact Point for eHealth node that issues cross-border messages.
* The *Responding NCP* : the National Contact Point for eHealth node that receives and processes cross-border messages.
* The *National Infrastructure* : the actual national infrastructure responsible for providing or receiving the data.

In these diagrams, the following aspects are not explicitly highlighted:
* The separation within the NCP between the National Connector component and the Common NCP node, which is typically implemented using the OpenNCP reference implementation.
* The actual application used by the Requestor to perform the described actions.


### The Sequence Diagrams

For each identified business process the following table describes the current implementation status and where to find the associated sequence diagram.

|       | Business Process         | Impl. Status      |  Sequence Diagram    |
|-------|--------------------------|-------------------| ------                 |
| 📝    | Patient Identification   | Pre-production    |  [Find Identity by Traits](#find-identity-by-traits)
| 💊    | Document Retrieval       | Pre-production    | [Document Retrieval](#document-retrieval) |
| 📄    | Document Provision       | Under definition  | [Document Provision](#document-provision) | 
| 💊    | Study Retrieval          | Under definition  | to be documented |
| 📄    | Images Retrieval         | Under definition  | to be documented |



#### Find Identity by Traits

The Patient Identification is a task used by almost all the use cases.

It is realized by searching patients by identifier and/or identification traits.


<div>
<p> </p>
{% include sequence-business-pat.svg %}
<p>Figure 1 - Find Identity by Traits</p>
<p> </p>
</div>


<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: Implementation details are provided in the 
            <a href="sequence-pat.html" target="_blank">
                Patient Search page
            </a>.
        </td>
    </tr>
</table>
</div>

#### Document Retrieval

The Document Retrieval may be realized by:
* searching and retrieving the documents
* fetching the documents

<div>
<p> </p>
{% include sequence-business-getDocs-details.svg %}
<p>Figure 2 - Document Retrieval</p>
<p> </p>
</div>


<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: Implementation details are provided in the 
            <a href="sequence-docs.html" target="_blank">
                DocumentReference Search page
            </a>.
        </td>
    </tr>
</table>
</div>

#### Document Provision

The Document Provision is realized by sending a document to the receiving  country (typycally the country of Affiliation)


<div>
<p> </p>
{% include sequence-app-sendDoc.svg %}
<p>Figure 3 - Document Provision</p>
<p> </p>
</div>

<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: Implementation details are provided in the 
            <a href="technical.html" target="_blank">
               XXXXXXX page
            </a>.
        </td>
    </tr>
</table>
</div>
