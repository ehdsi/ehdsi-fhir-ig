{% include fsh-link-references.md %}

This page describes the business scenarios for the **Hospital Discharege Report Use Case**.

This scenario can be realized by using a common interaction pattern describing the search and retrieval of a document.

Future specifications may differentiate the cases based on the search paramters used and the kind of data that is supposed to be seaerched and retrieved.

### Scenarios

#### Send Hospital Discharge report to country of Affiliation

1. Check Patient ID
1. HDR author creates and completes the discharge report of the Patient after the patient is discharged from the hospital and stores it in their EHR system. 
1. Responsible physician (attester) validates the HDR and Legal authenticator authenticates the HDR.
1. HDR sender electronically sends HDR linked to the Patient Identifier stored in the national system to the NCP of country B
1. The NCP of country B sends the HDR to the NCP of country A in the format specified by MyHealth@EU 
1. The NCP of country A sends the HDR to the national infrastructure to integrate the data received for the patient from country B in the national EHR systems of country A.
1. The NCP of country A sends back the confirmation of receipt of the HDR to the NCP of country B
1. The NCP of country B sends the confirmation receipt to the HDR sender
1. The use case is terminated.

#### Hospital Discharge Report sharing on a cross-border scale from country A to country B

1. Check Patient ID
1. Once the identity of the patient is validated, the HP of country B queries country A for a list of Hospital Discharge reports of that patient.
1. The HP interface requests the lists of Hospital Discharge Report of country A to the NCP of country B. 
1. The NCP of country B requests the list of HDR to the NCP of country A.
1. The NCP of country A, after checking if patient consent has been provided (where applicable), gets and provides to the NCP of country B the list of Hospital Discharge Report of country A.
1. The NCP of country B conveys the list to HP interface.
1. The HP-B selects all or subset of reports and requests them to the NCP of country B.
1. The NCP of country B request the selected reports to the NCP of country A.
1. The NCP of country A  gets and provides to the NCP of country B the requested HDR of country A in the MyHealth@EU format.
1. The NCP of country B conveys the HDR of country A to HP interface.
1. The HP-B accesses to the Patient's Hospital Discharge Report of country A.
1. The use case is terminated.

### Diagrams

<div>
<table border="1" style="width:100%; text-align:left;">
    <tr>
        <td>Note: click on the diagram to see more details about each interaction 
            <a href="application.html" target="_blank">
                [application view]
            </a>.
        </td>
    </tr>
</table>
</div>


#### Send Hospital Discharge report to country of Affiliation

{% include sequence-business-sendDocs.svg %}

#### Hospital Discharge Report sharing on a cross-border scale from country A to country B

{% include sequence-business-getDocs.svg %}

