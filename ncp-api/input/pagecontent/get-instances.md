{% include fsh-link-references.md %}

### MyHealth@EU Image Retrieval Service

#### Introduction
This page serves as the DICOM Conformance Statement for the MyHealth@EU DICOM Web Access to DICOM Objects over RESTful services (WADO-RS). It provides an overview of the DICOM capabilities supported by the service, specifically focusing on the retrieval of DICOM instances at the Study, Series, and Instance levels.

#### Overview of DICOM WADO-RS
The DICOM Web Access to DICOM Persistent Objects by RESTful Services (WADO-RS) is a web service that allows for the retrieval of DICOM objects using standard HTTP protocols. The MyHealth@EU WADO-RS service conforms to the DICOM standard for web-based access to DICOM instances.

#### Conformance to DICOM Standards

##### DICOM Standard Compliance
The MyHealth@EU WADO-RS service is compliant with the following DICOM standards:
- **DICOM PS 3.18 (Web Services):** The service adheres to the RESTful web services specifications outlined in DICOM PS 3.18 for the retrieval of DICOM objects.

##### Supported DICOM SOP Classes
The service supports retrieval for the following DICOM SOP Classes:

| SOP Class | SOP Class UID |
|-----------------------------|------------------------------|
| CT Image Storage | 1.2.840.10008.5.1.4.1.1.2 |
| MR Image Storage | 1.2.840.10008.5.1.4.1.1.4 |
| Ultrasound Image Storage | 1.2.840.10008.5.1.4.1.1.6.1 |
| Secondary Capture Image Storage | 1.2.840.10008.5.1.4.1.1.7 |
| Digital Mammography X-Ray Image Storage - For Presentation | 1.2.840.10008.5.1.4.1.1.1.2 |
| Positron Emission Tomography (PET) Image Storage | 1.2.840.10008.5.1.4.1.1.128 |
| Enhanced MR Image Storage | 1.2.840.10008.5.1.4.1.1.4.1 |
| Other relevant SOP Classes as applicable to your service | [SOP Class UID] |

##### Supported DICOM Operations
The service supports the following DICOM WADO-RS operations:
- **Retrieve Study:** Allows retrieval of all DICOM instances within a specified study.
- **Retrieve Series:** Allows retrieval of all DICOM instances within a specified series.
- **Retrieve Instance:** Allows retrieval of a specific DICOM instance.

##### Supported Transfer Syntaxes
The service supports the following DICOM Transfer Syntaxes for the retrieval of DICOM objects:

| Transfer Syntax | Transfer Syntax UID |
|---------------------------|--------------------------------|
| Implicit VR Little Endian | 1.2.840.10008.1.2 |
| Explicit VR Little Endian | 1.2.840.10008.1.2.1 |
| Explicit VR Big Endian | 1.2.840.10008.1.2.2 |
| JPEG Baseline (Process 1) | 1.2.840.10008.1.2.4.50 |
| JPEG Lossless, Non-Hierarchical (Process 14) | 1.2.840.10008.1.2.4.70 |
| JPEG 2000 Image Compression (Lossless Only) | 1.2.840.10008.1.2.4.90 |
| JPEG 2000 Image Compression | 1.2.840.10008.1.2.4.91 |
| RLE Lossless | 1.2.840.10008.1.2.5 |

#### WADO-RS Service Details

##### Service Endpoints
The MyHealth@EU WADO-RS service provides the following endpoints for DICOM object retrieval:
- **Study Level Retrieval:** `GET /studies/{studyInstanceUID}`
- **Series Level Retrieval:** `GET /studies/{studyInstanceUID}/series/{seriesInstanceUID}`
- **Instance Level Retrieval:** `GET /studies/{studyInstanceUID}/series/{seriesInstanceUID}/instances/{sopInstanceUID}`

##### Supported Media Types
The service supports the following media types for DICOM instance retrieval:
- **application/dicom:** Returns the DICOM object in its native format.
- **image/jpeg:** Returns the DICOM object as a JPEG image (where applicable).
- **application/octet-stream:** Returns the DICOM object as a binary stream.

##### Authentication and Authorization
The WADO-RS service requires *To be Defined* for secure access to DICOM instances. Users must be authenticated and authorized to retrieve DICOM objects.

#### Compliance Notes

- The service is designed to be compliant with GDPR and other relevant European regulations for the secure handling of medical data.
- The service adheres to the DICOM standard for the encoding and transfer of DICOM objects over web services.

#### Limitations and Assumptions

- The service currently supports retrieval of images and associated metadata. **Advanced retrieval options such as retrieving specific frames or applying presentation states are not supported in this version**.
- The service assumes that the client application is capable of interpreting DICOM data and displaying it appropriately.

