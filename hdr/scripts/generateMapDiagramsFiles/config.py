# config.py

# Specify the directory containing the scripts
# config.py
scripts_dir = r'C:\workspace\___Python\gen-map-diag'


# Specify the output CSV file name
csv_file = r'C:\gitlab\ehdsi-fhir-ig-master\hdr\scripts\generateMapDiagramsFiles\source_to_target_model_table.csv'

# ### EPS HL7 Eu ###
directory_path = r'C:\gitlab\ehdsi-fhir-ig-master\hdr\fsh-generated\resources'
puml_dir = r'C:\gitlab\ehdsi-fhir-ig-master\hdr\input\images-source'
xml_output_dir = r'C:\gitlab\ehdsi-fhir-ig-master\hdr\input\pagecontent'
template_file_path = r'C:\gitlab\ehdsi-fhir-ig-master\hdr\scripts\generateMapDiagramsFiles\template-eu-hdr.xhtml'  # Update this with your template file path

