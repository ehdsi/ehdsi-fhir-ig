{% include fsh-link-references.md %}

<div xmlns="http://www.w3.org/1999/xhtml"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <blockquote class="stu-note">  
  <p>Obligations have been added to this version of the guide only as <b>Informative</b> material to collect feedback about their usage.</p>  
  <p>For more details about obligations please refer to the <a href="obligations.html">Obligations page</a></p>
 </blockquote>
</div>

### Scope

Specify a set of rules to be applied to HL7 FHIR to define how to represent a **Hospital Discharge Report** for the **European cross-borders exchange**, coherently with the European eHN Guidelines (see the [European eHealth - Key documents](https://health.ec.europa.eu/ehealth-digital-health-and-care/key-documents_en) ).

This guide is derived for the HL7 FHIR IG fulfilling the MyHealth@EU Functional Requirements (see the [MyHealth@EU Requirements](requirements.html) page)
 
### MyHealth@EU

The Use Case originates from the Change Proposal produced by the X-eHealth project/MyHealth@EU New Use Case Workgroup, and the details are available in the corresponding Change Proposal: CP-eHealthDSI-073: Implement the new service Laboratory Result Report (corresponding to the Wave 8 of MyHealth@EU).

The corresponding eHN Guidelines: eHN Laboratory Result Guidelines (release 1) are available [here](https://health.ec.europa.eu/publications/ehn-laboratory-result-guidelines-release-1_en)

### Design choices

### Navigating the profiles

### Dependencies

{% include dependency-table.xhtml %}

### Cross Version Analysis

{% include cross-version-analysis.xhtml %}

### Global Profiles

{% include globals-table.xhtml %}

### IP statements

{% include ip-statements.xhtml %}
