Profile: LocationHdrMyHealthEu
Parent: Location // http://hl7.org/fhir/uv/ips/StructureDefinition/Organization-uv-ips
Id: location-hdr-myhealtheu
Title: "Location (HDR)"
Description: "This profile sets minimum expectations for the Location resource to be used for the purpose of this guide."

* insert SetFmmandStatusRule (1, draft)

* physicalType ^short = "Location type"
* name ^short = "Location name"
* telecom ^short = "Location telecom"
* address only AddressEu