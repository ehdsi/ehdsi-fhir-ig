Profile: BundleHdrMyHealthEu
Parent: $bundle-eu-hdr
Id: bundle-hdr-myhealtheu
Title: "Bundle: Hospital Discharge Report"
Description: "Clinical document used to represent a Hospital Discharge Report for the scope of MyHealth@EU."
* . ^short = "Hospital Discharge Report bundle"
* . ^definition = "Hospital Discharge Report bundle."

// * obeys bdl-hdr-1

* identifier 1.. 
  * ^short = "Instance identifier"
* type = #document (exactly)
* timestamp 1.. 
  * ^short = "Instance identifier"
* link ..0


* entry.fullUrl 1..
* entry.search ..0
* entry.request ..0
* entry.response ..0

* entry.resource 1..

* entry[composition].resource only CompositionHdrMyHealthEu
* entry[patient].resource only PatientHdrMyHealthEu // EU ?
* entry[allergyIntolerance].resource only AllergyIntoleranceHdrMyHealthEu
* entry[condition].resource only $condition-eu-hdr
* entry[device].resource only $device-eu-hdr
* entry[deviceUseStatement].resource only $deviceUseStatement-eu-hdr
* entry[diagnosticReport].resource only $DiagnosticReport-uv-ips
* entry[encounter].resource only EncounterHdrMyHealthEu
* entry[imagingStudy].resource only $ImagingStudy-uv-ips
* entry[immunization].resource only $Immunization-uv-ips
* entry[media].resource only Media // $Media-observation-uv-ips
// * entry[medication].resource only Medication //HdrMyHealthEu
// * entry[medicationRequest].resource only MedicationRequest // HdrMyHealthEu
* entry[medicationStatement].resource only $medicationStatement-eu-hdr
* entry[medicationAdministration].resource only $medicationAdministration-eu-hdr
* entry[medicationDispense].resource only $medicationDispense-eu-hdr
* entry[practitioner].resource only PractitionerHdrMyHealthEu
* entry[practitionerRole].resource only PractitionerRoleHdrMyHealthEu
* entry[procedure].resource only $procedure-eu-hdr
* entry[organization].resource only OrganizationHdrMyHealthEu
* entry[observation].resource only Observation // $Observation-results-uv-ips
* entry[specimen].resource only $Specimen-uv-ips

* entry[flag].resource only Flag
* entry[familyMemberHistory].resource only FamilyMemberHistory
* entry[documentReference].resource only DocumentReference

* signature ^short = "Report Digital Signature"
  * type ^short = "Digital Signature Purposes"
  * when ^short = "When was signed"
  * who ^short = "Who signed."
  * data ^short = "Signature content"