Profile: CompositionHdrMyHealthEu
Parent: $composition-eu-hdr // add dependency form the $composition-eu-hdr
Id: composition-hdr-myhealtheu
Title: "Composition: Hospital Discharge Report"
Description: "Clinical document used to represent a Hospital Discharge Report (HDR) for the scope of MyHealth@EU."
* . ^short = "Hospital Discharge Report composition"
* . ^definition = "Hospital Discharge Report composition. \r\nA composition is a set of healthcare-related information that is assembled together into a single logical document that provides a single coherent statement of meaning, establishes its own context and that has clinical attestation with regard to who is making the statement. \r\nWhile a Composition defines the structure, it does not actually contain the content: rather the full content of a document is contained in a Bundle, of which the Composition is the first resource contained."


* extension[information-recipient] contains practictionerRole 0..*
* extension[information-recipient][practictionerRole].valueReference only Reference( PractitionerRoleHdrMyHealthEu or PractitionerHdrMyHealthEu or Device or PatientHdrMyHealthEu or RelatedPerson or  OrganizationHdrMyHealthEu)

* encounter only Reference (EncounterHdrMyHealthEu)

* date ^short = "HDR date"
* author ^short = "Who and/or what authored the Hospital Discharge Report"
* author only Reference ( PractitionerRoleHdrMyHealthEu or PractitionerHdrMyHealthEu or PatientHdrMyHealthEu or RelatedPerson or OrganizationHdrMyHealthEu)

* title ^short = "Hospital Discharge Report"
* title ^definition = "Official human-readable label for the composition.\r\n\r\nFor this document should be \"Hospital Discharge Report\" or any equivalent translation"
* attester.mode ^short = "The type of attestation"
* attester.time ^short = "When the composition was attested"
* attester.party ^short = "Who attested the composition"