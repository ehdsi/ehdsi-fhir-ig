
//====== Profiles =====================================

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Profile:  PractitionerHdrMyHealthEu
Parent:   Practitioner // http://hl7.org/fhir/uv/ips/StructureDefinition/Practitioner-uv-ips
Id:       practitioner-hdr-myhealtheu
Title:    "Practitioner (HDR)"
Description: "This profile defines how to represent Practitioners in FHIR for the purpose of the  HL7 Europe project."
//-------------------------------------------------------------------------------------------
* insert SetFmmandStatusRule (1, draft)
* insert ImposeProfile ($practitioner-eu-core, 0)
* identifier ^short = "Practitioner identifier"
* name ^short = "Practitioner name"
//-------------------------------------------------------------------------------------------

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Profile:  PractitionerRoleHdrMyHealthEu
Parent:   PractitionerRole // http://hl7.org/fhir/uv/ips/StructureDefinition/PractitionerRole-uv-ips
Id:       practitionerRole-hdr-myhealtheu
Title:    "PractitionerRole: (HDR)"
Description: "This profile defines how to represent Practitioners (with their roles) in FHIR for the purpose of the  HL7 Europe project."
//-------------------------------------------------------------------------------------------

* insert SetFmmandStatusRule (1, draft)
* insert ImposeProfile ($practitionerRole-eu-core, 0)

* identifier ^short = "Business identifier"
* organization only Reference (OrganizationHdrMyHealthEu)
* practitioner only Reference (PractitionerHdrMyHealthEu)
* code from EHDSIHealthcareProfessionalRole
//-------------------------------------------------------------------------------------------
