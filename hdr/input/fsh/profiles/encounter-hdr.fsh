Profile: EncounterHdrMyHealthEu
Parent: $encounter-eu-hdr
Id: encounter-hdr-myhealtheu
Title:    "Encounter (HDR)"
Description: "This profile defines how to represent Inpatient Encounter in HL7 FHIR for the scope of this guide."
* identifier ^short = "Identifier(s) by which this encounter is known."
* status = #finished  // always #finished in discharge letter
  * ^short = "Status of this Hospital stay"
  * ^definition = "At the discharge report status of the encounter should be always = \"finished\""
* class ^short = "check value set binding"
* type ^short = "check value set binding"
* serviceType ^short = "check value set binding"
* priority ^short = "check value set binding"
* subject only Reference (PatientHdrMyHealthEu)
* subject 1..
  * ^short = "The patient present at the encounter"
* basedOn ^short = "The request for which this encounter has been made"
* period
  * ^short = "The start and end time of this encounter."
  * ^definition = "The start and end time of this inpatient stay."
* reasonCode ^short = "check value set binding"
* reasonReference only Reference ( Observation or Condition or Procedure)

* participant ^short = "Add appropriate references to Practitioner and PractitionerRole"
  * individual only Reference (PractitionerHdrMyHealthEu or $practitionerRole-eu-core or RelatedPerson)

  * participant
  * ^short = "List of participants involved in the encounters."
  * ^definition = """Slice per type of participant: admitter, discharger,..."""

* participant[admitter]
  * ^short = "Admitting professional."
  * type = $v3-ParticipationType#ADM

* participant[discharger]
  * ^short = "Discharging professional."
  * type = $v3-ParticipationType#DIS

* participant[referrer]
  * ^short = "Referring professional."
  * type = $v3-ParticipationType#REF



* diagnosis ^short = "The list of diagnosis relevant to this encounter, see comment"
* diagnosis ^comment = "While Encounter.diagnosis could be optionally populated, mainly for administrative purposes, we strongly recommend to put all clinical relevant diagnoses, stated at start, during and at the end of the hospital stay, into the corresponding section(s) of the HDR."
* diagnosis.condition only Reference(Condition)


* hospitalization
  * admitSource ^short = "From where patient was admitted (physician referral, transfer)."
  * dischargeDisposition ^short = "Category or kind of location after discharge"
  * destination only Reference (OrganizationHdrMyHealthEu or LocationEuHdr)
  // add voc binding

* location ^short = "Locations where the patient stayed"
  * location only Reference ( LocationEuHdr )
  * period ^short = "Location period"

* serviceProvider only Reference ( OrganizationHdrMyHealthEu )