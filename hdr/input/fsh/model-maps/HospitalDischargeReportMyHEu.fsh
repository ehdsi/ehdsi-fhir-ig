Logical: HospitalDischargeReportMyHEu
Id: HospitalDischargeReport
Title: "A - Hospital Discharge Report (eHN)"
Description: """Hospital Discharge Report"""
* header 1..1 Header "A.1 - Hospital Discharge Report header data element" """Hospital Discharge Report header data element"""
* body 1..1 BackboneElement "A.2 - Hospital Discharge Report body data element" """Hospital Discharge Report body data element"""
* body.alerts 0..1 Alerts "A.2.2 - Alerts" """Alerts"""
* body.encounter 1..1 InPatientEncounter "A.2.3 - Encounter" """Encounter """
* body.hospitalStay 0..1 HospitalStay "A.2.6 - Course of hospitalisation (Hospital stay)" """Course of hospitalisation (Hospital stay)"""
* body.dischargeDetails 0..1 DischargeDetails "A.2.7 - Discharge details" """Discharge details (structured information should be provided, however if not available, at least a summary note should be present)."""
* body.recommendations 0..1 Recommendations "A.2.8 - Care plan and other recommendations after discharge." """Care plan and other recommendations after discharge."""
