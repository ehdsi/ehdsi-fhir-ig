Instance: hdr2FHIR-hdr-myhealtheu
InstanceOf: ConceptMap
Usage: #definition
* url = "http://fhir.ehdsi.eu/hdr/ConceptMap/hdr2FHIR-hdr-myhealtheu"
* name = "Hdr2FHIRHdrMyHEu"
* title = "eHN HDR Model to this guide Map"
* status = #draft
* experimental = true
* description = """eHN Hospital Discharge Report Model to this guide mapping"""
* group[+].source = "http://fhir.ehdsi.eu/hdr/StructureDefinition/HospitalDischargeReport"
* group[=].target = "http://fhir.ehdsi.eu/hdr/StructureDefinition/bundle-hdr-myhealtheu"
* group[=].element[+].code = #HospitalDischargeReport.header
* group[=].element[=].display = "A.1 - Hospital Discharge Report header data element"
* group[=].element[=].target.code = #Bundle
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "See the header model and map for details"
* group[+].source = "http://fhir.ehdsi.eu/hdr/StructureDefinition/HospitalDischargeReport"
* group[=].target = "http://fhir.ehdsi.eu/hdr/StructureDefinition/composition-hdr-myhealtheu"
* group[=].element[+].code = #HospitalDischargeReport.body
* group[=].element[=].display = "A.2 - Hospital Discharge Report body data element"
* group[=].element[=].target.code = #Composition
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "See details below"
* group[=].element[+].code = #HospitalDischargeReport.body.alerts
* group[=].element[=].display = "A.2.2 - Alerts"
* group[=].element[=].target.code = #Composition.section:alertSection
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "See the Alerts model and map for details"
* group[=].element[+].code = #HospitalDischargeReport.body.encounter
* group[=].element[=].display = "A.2.3 - Encounter"
* group[=].element[=].target.code = #Composition.encounter
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "See the InPatientEncounter model and map for details"
* group[=].element[+].code = #HospitalDischargeReport.body.hospitalStay
* group[=].element[=].display = "A.2.6 - Course of hospitalisation (Hospital stay)"
* group[=].element[=].target.code = #Composition.section:sectionHospitalCourse
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "TO BE REVIEWED"
* group[=].element[+].code = #HospitalDischargeReport.body.dischargeDetails
* group[=].element[=].display = "A.2.7 - Discharge details"
* group[=].element[=].target.code = #Composition.section:dischargeDetails
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "TO BE REVIEWED"
* group[=].element[+].code = #HospitalDischargeReport.body.recommendations
* group[=].element[=].display = "A.2.8 - Care plan and other recommendations after discharge."
* group[=].element[=].target.code = #Composition.section:sectionPlanOfCare
* group[=].element[=].target.display = ""
* group[=].element[=].target.equivalence = #relatedto
* group[=].element[=].target.comment = "TO BE REVIEWED"
