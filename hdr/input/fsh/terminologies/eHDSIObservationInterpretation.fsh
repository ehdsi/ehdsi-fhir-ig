ValueSet:      EHDSIObservationInterpretation
Id:	       eHDSIObservationInterpretation
Title:	       "eHDSI Observation Interpretation"
Description:   "This Value Set is used for a rough qualitative interpretation of the Laboratory Observation Results"
* ^status = #draft
* ^experimental = false

* insert SetFmmandStatusRule ( 1, draft )
* insert setOID ( 1.3.6.1.4.1.12559.11.10.1.3.1.42.73 )
* insert setUrl ( eHDSIObservationInterpretation )

* $v3-ObservationInterpretation#< "Off scale low"
* $v3-ObservationInterpretation#> "Off scale high"
* $v3-ObservationInterpretation#A "Abnormal"
* $v3-ObservationInterpretation#AA "Critical abnormal"
* $v3-ObservationInterpretation#B "Better"
* $v3-ObservationInterpretation#CAR "Carrier"
* $v3-ObservationInterpretation#D "Significant change down"
* $v3-ObservationInterpretation#DET "Detected"
* $v3-ObservationInterpretation#E "Equivocal"
* $v3-ObservationInterpretation#EX "outside threshold"
* $v3-ObservationInterpretation#EXP "Expected"
* $v3-ObservationInterpretation#H "High"
* $v3-ObservationInterpretation#HH "Critical high"
* $v3-ObservationInterpretation#HU "Significantly high"
* $v3-ObservationInterpretation#HX "above high threshold"
* $v3-ObservationInterpretation#I "Intermediate"
* $v3-ObservationInterpretation#IE "Insufficient evidence"
* $v3-ObservationInterpretation#IND "Indeterminate"
* $v3-ObservationInterpretation#L "Low"
* $v3-ObservationInterpretation#LL "Critical low"
* $v3-ObservationInterpretation#LU "Significantly low"
* $v3-ObservationInterpretation#LX "below low threshold"
* $v3-ObservationInterpretation#N "Normal"
* $v3-ObservationInterpretation#NCL "No CLSI defined breakpoint"
* $v3-ObservationInterpretation#ND "Not detected"
* $v3-ObservationInterpretation#NEG "Negative"
* $v3-ObservationInterpretation#NR "Non-reactive"
* $v3-ObservationInterpretation#NS "Non-susceptible"
* $v3-ObservationInterpretation#POS "Positive"
* $v3-ObservationInterpretation#R "Resistant"
* $v3-ObservationInterpretation#RR "Reactive"
* $v3-ObservationInterpretation#S "Susceptible"
* $v3-ObservationInterpretation#U "Significant change up"
* $v3-ObservationInterpretation#UNE "Unexpected"
* $v3-ObservationInterpretation#W "Worse"
* $v3-ObservationInterpretation#WR "Weakly reactive"
* $v3-ObservationInterpretation#SYN-R "Synergy - resistant"
* $v3-ObservationInterpretation#SDD "Susceptible-dose dependent"
* $v3-ObservationInterpretation#SYN-S "Synergy - susceptible"