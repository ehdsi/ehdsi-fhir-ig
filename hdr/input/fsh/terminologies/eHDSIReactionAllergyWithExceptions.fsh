// -------------------------------------------------------------------------------					
//  Value Set. File: 					eHDSIReactionAllergyWithExceptions.fsh
// -------------------------------------------------------------------------------					
ValueSet: EHDSIReactionAllergyWithExceptions
Id: eHDSIReactionAllergyWithExceptions
Title: "eHDSI Reaction allergy with exceptions"
Description: "The Value Set is used to allergy reaction manifestation. It also includes exceptional values."
// * url = "http://terminology.hl7.it/ConceptMap/ConceptMap-subject2osiris"					
// * ^status = #active					
* ^experimental = false

* insert SNOMEDCopyrightForVS
* insert SetFmmandStatusRule ( 1, draft )

* codes from valueset EHDSIExceptionalValue
* codes from valueset EHDSIReactionAllergy