ValueSet:      EHDSIReferenceRangeMeaning
Id:	       	   eHDSIReferenceRangeMeaning
Title:	       "eHDSI Reference Range Meaning"
Description:   "This value set defines a set of codes for a reference range qualifier"

* ^experimental = false

* insert SetFmmandStatusRule ( 1, draft )
* insert setOID ( 1.3.6.1.4.1.12559.11.10.1.3.1.42.74 )
* insert setUrl ( eHDSIReferenceRangeMeaning )

// * ^url = https://terminology.ehdsi.eu/ValueSets/eHDSIReferenceRangeMeaning
* $referencerange-meaning#normal "Normal Range"
* $referencerange-meaning#recommended "Recommended Range"
* $referencerange-meaning#treatment "Treatment Range"
* $referencerange-meaning#therapeutic "Therapeutic Desired Level"
* $referencerange-meaning#pre-puberty "Pre-Puberty"
* $referencerange-meaning#follicular "Follicular Stage"
* $referencerange-meaning#midcycle "MidCycle"
* $referencerange-meaning#luteal "Luteal"
* $referencerange-meaning#postmenopausal "Post-Menopause"
* $referencerange-meaning#pre "Pre Therapeutic Desired Level"
* $referencerange-meaning#post "Post Therapeutic Desired Level"