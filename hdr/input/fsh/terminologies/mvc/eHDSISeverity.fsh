// -------------------------------------------------------------------------------
//  Value Set. File: 					eHDSISeverity.fsh
// -------------------------------------------------------------------------------

ValueSet: EHDSISeverity
Id: eHDSISeverity
Title: "eHDSI Severity"
Description: "The Value Set is used for all Problems and Allergies in the Patient Summary to indicate the severity of the problem (or Allergy)"
* ^experimental = false

* insert SNOMEDCopyrightForVS
* insert SetFmmandStatusRule ( 1, draft )
* insert setOID (1.3.6.1.4.1.12559.11.10.1.3.1.42.13)
* insert setUrl ( eHDSISeverity)

*  $sct#6736007 "Moderate"
*  $sct#371924009 "Moderate to severe"
*  $sct#24484000 "Severe"
*  $sct#255604002 "Mild"
*  $sct#371923003 "Mild to moderate"
*  $sct#442452003 "Life threatening severity"
