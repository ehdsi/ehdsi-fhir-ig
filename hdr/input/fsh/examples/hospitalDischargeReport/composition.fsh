// Define an instance of the HDR Composition for MyHealth@EU
Instance: ExampleHDRComposition
InstanceOf: CompositionHdrMyHealthEu
Title: "Example Hospital Discharge Report"
Description: "A Hospital Discharge Report for cross-border healthcare in the EU."
Usage: #inline
* id = "83b8f6d4-b345-4673-a127-59131ac352c9"
* status = #final
* type = $loinc#34105-7 "Hospital Discharge summary"
* subject = Reference(urn:uuid:1d4bbc93-63b7-4f2b-8f0f-8380aa138f49)
* date = "2024-11-12"
* author[+].display = "Dr. Patrick Dempsey"
* title = "Hospital Discharge Report - 11 November, 2024 17:20"
* text.status = #generated
* text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Narrative Hospital Discharge Report</div>"
* encounter =  Reference(urn:uuid:b49cdbed-299c-4b95-aa9d-273811521147) 

// Course of Hospitalization (Hospital stay)
* section[sectionHospitalCourse].code = $loinc#8648-8 "Hospital course Narrative"
* section[sectionHospitalCourse].title = "Course of hospitalization (Hospital stay)"
* section[sectionHospitalCourse].text.status = #generated
* section[sectionHospitalCourse].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Hospital course Narrative</div>"
// * section[sectionHospitalCourse].entry[+] = Reference(ExampleHDREncounter)
* section[sectionDiagnosticSummary].code = $loinc#11450-4
* section[sectionDiagnosticSummary].title = "Problem list - Reported"
* section[sectionDiagnosticSummary].text.status = #generated
* section[sectionDiagnosticSummary].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Diagnostic Summary Description Narrative</div>"
* section[sectionSignificantProcedures].code = $loinc#10185-7 "Hospital discharge procedures Narrative"
* section[sectionSignificantProcedures].title = "Significant Procedures Description"
* section[sectionSignificantProcedures].text.status = #generated
* section[sectionSignificantProcedures].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Significant Procedures Narrative</div>"
* section[sectionSignificantProcedures].entry[+] = Reference(urn:uuid:457d661f-98b6-493a-b173-0e91395b5545)


* section[sectionMedicalDevices].code = $loinc#46264-8 "History of medical device use"
* section[sectionMedicalDevices].title = "Medical Devices Description"
* section[sectionMedicalDevices].text.status = #generated
* section[sectionMedicalDevices].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Medical Devices Narrative</div>"
* section[sectionMedicalDevices].entry[+] = Reference(urn:uuid:32ce04ab-fe6c-4fca-b557-def9f86c5462) 


* section[sectionPharmacotherapy].code = $loinc#10160-0 "History of Medication use Narrative"
* section[sectionPharmacotherapy].title = "Medications description"
* section[sectionPharmacotherapy].text.status = #generated
* section[sectionPharmacotherapy].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Medications Narrative</div>"
* section[sectionPharmacotherapy].entry[+] = Reference(urn:uuid:b46f7abd-6e20-44e7-bf9a-a0bd9685fd3e)

* section[sectionSynthesis].code = $loinc#51848-0 "Evaluation note"
* section[sectionSynthesis].title = "Synthesis Description"
* section[sectionSynthesis].text.status = #generated
* section[sectionSynthesis].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Synthesis Narrative</div>"


/* * section[sectionSynthesis].section[sectionProblemSynthesis].code = $sct#423016009
* section[sectionSynthesis].section[sectionProblemSynthesis].title = "Synthesis - Problem Synthesis Description"
* section[sectionSynthesis].section[sectionProblemSynthesis].text.status = #generated
* section[sectionSynthesis].section[sectionProblemSynthesis].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Synthesis - Problem Synthesis Narrative</div>"
* section[sectionSynthesis].section[sectionClinicalReasoning].title = "Synthesis - Clinical Reasoning Description"
* section[sectionSynthesis].section[sectionClinicalReasoning].text.status = #generated
* section[sectionSynthesis].section[sectionClinicalReasoning].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Synthesis - Clinical Reasoning Narrative</div>" */

// Discharge details
* section[sectionDischargeDetails].code = $loinc#8650-4 "Hospital discharge disposition Narrative"
* section[sectionDischargeDetails].title = "Discharge details"
* section[sectionDischargeDetails].text.status = #generated
* section[sectionDischargeDetails].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Discharge Details Narrative</div>"

// Section Allergies and Intolerances
* section[sectionAllergies].code = $loinc#48765-2 "Allergies and adverse reactions Document"
* section[sectionAllergies].title = "Allergies and Intolerances"

* section[sectionAllergies].text.status = #generated
* section[sectionAllergies].text.div = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Allergies and Intolerances section Narrative</div>"
* section[sectionAllergies].entry[+] = Reference(urn:uuid:5b87ea1e-01ad-4239-81ec-3a16b5c4f4ec)


Instance: ExampleDeviceUseStatement
InstanceOf: DeviceUseStatement
Title: "Example Device Use Statement"
Description: "An example of a DeviceUseStatement resource."
Usage: #inline
* id = "32ce04ab-fe6c-4fca-b557-def9f86c5462"
* status = #active
* subject = Reference(urn:uuid:1d4bbc93-63b7-4f2b-8f0f-8380aa138f49) // Replace with the ID of a Patient instance
* device = Reference(urn:uuid:a53bdf53-9b4b-4b9d-aaab-ac346c61ea94)  // Replace with the ID of a Device instance


Instance: ExampleDevice
InstanceOf: Device
Title: "Example Medical Device"
Description: "An example instance of a medical device resource."
Usage: #inline
* id = "a53bdf53-9b4b-4b9d-aaab-ac346c61ea94"
* status = #active
* type.coding[0].code = $sct#86184003 "Blood pressure cuff"
* manufacturer = "ACME Medical Devices Inc."
* deviceName[0].name = "SuperCuff 3000"
* deviceName[0].type = #manufacturerName
* modelNumber = "SC3000"
* serialNumber = "12345-67890"
* partNumber = "98765-43210"
* property[0].type.coding[0].code = http://example.org/device-properties#battery-life
* property[0].valueQuantity.value = 10
* property[0].valueQuantity.unit = "hours"
* owner = Reference(urn:uuid:ff4d98d1-69c9-43db-896e-58d11c15662c)
* patient = Reference(urn:uuid:1d4bbc93-63b7-4f2b-8f0f-8380aa138f49)
* udiCarrier[0].deviceIdentifier = "12345678901234"
* udiCarrier[0].issuer = "http://hl7.org/fhir/NamingSystem/gs1"
* udiCarrier[0].jurisdiction = "http://hl7.org/fhir/ValueSet/iso3166-1-2"

Instance: ExampleAllergyIntolerance
InstanceOf: AllergyIntoleranceHdrMyHealthEu
Title: "Example AllergyIntolerance for EU-EPS"
Description: "An example instance of AllergyIntolerance conforming to the EU-EPS profile."
Usage: #inline

* id = "5b87ea1e-01ad-4239-81ec-3a16b5c4f4ec"
* clinicalStatus = #active
* verificationStatus = #confirmed
* type = #allergy
* category[0] = #food
* criticality = #high
* code = $sct#91936005 "Peanut allergy"
* patient = Reference(urn:uuid:1d4bbc93-63b7-4f2b-8f0f-8380aa138f49) // Link to a Patient instance
* onsetDateTime = "2023-10-01T12:00:00+01:00"
* recordedDate = "2023-10-15T09:30:00+01:00"
* recorder = Reference(urn:uuid:b06b2c2c-15dd-4e58-b27e-37326ba51407) // Link to a Practitioner instance
* reaction[0].substance = $sct#227493005 "Peanut"
* reaction[0].manifestation[0] = $sct#271807003 "Skin rash"
* reaction[0].severity = #24484000 "Severe"

Instance: ExampleProcedure
InstanceOf: $Procedure-uv-ips
Title: "Example Procedure"
Description: "An example Procedure instance conforming to the HL7 FHIR R4 specification."
Usage: #inline

* id = "457d661f-98b6-493a-b173-0e91395b5545"
* status = #completed
* code = $sct#80146002 "Appendicectomy"
* subject = Reference(urn:uuid:1d4bbc93-63b7-4f2b-8f0f-8380aa138f49) // Reference to a Patient instance
* performedPeriod.start = "2023-11-01T10:00:00+01:00"
* performedPeriod.end = "2023-11-01T11:30:00+01:00"
* performer[0].actor = Reference(urn:uuid:b06b2c2c-15dd-4e58-b27e-37326ba51407) // Reference to a Practitioner instance
* recorder = Reference(urn:uuid:b06b2c2c-15dd-4e58-b27e-37326ba51407) // Reference to a Practitioner who recorded the procedure
* reasonCode[0] = $sct#85189001 "Acute appendicitis"
* bodySite[0] = $sct#66754008 "Appendix structure"
* outcome = $procedure-outcome#successful "Successful"

Instance: ExampleMedicationStatement
InstanceOf: $MedicationStatement-uv-ips
Title: "Example MedicationStatement"
Description: "An example instance of a MedicationStatement conforming to the HL7 FHIR R4 specification."
Usage: #inline

* id = "b46f7abd-6e20-44e7-bf9a-a0bd9685fd3e"
* status = #active
* statusReason[0] = $medication-statement-status-reason#on-hold "On hold"
* medicationCodeableConcept = http://www.nlm.nih.gov/research/umls/rxnorm#1049624 "Atorvastatin 10 MG Oral Tablet"
* subject = Reference(urn:uuid:1d4bbc93-63b7-4f2b-8f0f-8380aa138f49) // Reference to a Patient instance
* effectiveDateTime = "2023-11-01T08:00:00+01:00"
* dateAsserted = "2023-11-15T12:00:00+01:00"
* informationSource = Reference(urn:uuid:b06b2c2c-15dd-4e58-b27e-37326ba51407) // Reference to a Practitioner instance
* reasonCode[0] = $sct#44054006 "Hypercholesterolemia"
* dosage[0].text = "Take one tablet daily in the morning."
* dosage[0].timing.repeat.frequency = 1
* dosage[0].timing.repeat.period = 1
* dosage[0].timing.repeat.periodUnit = #d
* dosage[0].route = $sct#26643006 "Oral route"

Instance: ExamplePractitioner
InstanceOf: PractitionerHdrMyHealthEu
Title: "Example Practitioner"
Description: "An example of a Practitioner instance conforming to PractitionerEuHdr."
Usage: #inline

* id = "5947e818-0a8c-4f3d-b900-d933da7ed15d"
* identifier.system = "http://example.org/practitioner-ids"
* identifier.value = "123456789"
* name.family = "Doe"
* name.given = "John"
* telecom.system = #email
* telecom.value = "j.doe@example.org"
* telecom.use = #work
/* * address[0].line = "123 Example Street"
* address[0].city = "Sampletown"
* address[0].postalCode = "12345"
* address[0].country = "Exampleland" */

Instance: ExamplePractitionerRole
InstanceOf: PractitionerRoleHdrMyHealthEu
Title: "Example PractitionerRole"
Description: "An example of a PractitionerRole instance conforming to PractitionerRoleEuHdr."
Usage: #inline

* id = "b06b2c2c-15dd-4e58-b27e-37326ba51407"
* identifier.system = "http://example.org/role-ids"
* identifier.value = "role-123"
* practitioner = Reference(urn:uuid:5947e818-0a8c-4f3d-b900-d933da7ed15d) // Linking the Practitioner instance
* organization = Reference(urn:uuid:ff4d98d1-69c9-43db-896e-58d11c15662c) // Replace with an actual organization instance
* code = http://terminology.hl7.org/CodeSystem/practitioner-role#doctor "Doctor"
* specialty = http://hl7.org/fhir/specialty#cardio "Cardiology"
* telecom[0].system = #phone
* telecom[0].value = "+123456789"
* telecom[0].use = #work
// * location[0] = Reference(ExampleLocation) // Replace with an actual location instance

