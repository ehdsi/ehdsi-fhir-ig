
### Overview 

This guide provides a representation of the Laboratory Result Content specified in the MyHealth@EU Functional Requirements [13.01 Create the MyHealth@EU Laboratory Result Report Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/13.01+Create+the+MyHealth@EU+Laboratory+Result+Report+Content) as HL7 FHIR Logical Model.

This representation has been made based on the following approach:
1) the Conceptual grouping has been maintained as possible by using the element nesting.
2) Cardinality (required in the HL7 FHIR LMs) have been specifed based on the element optionality. That is 1..1/1..* is used only for Mandatory elments.
3) Required element have been marked by using the "handle" Obligation. Future requirements may use obligations to more specifically specify the functional expectations (e.g. should display; should populate;...)

the following table summarizes the mapping 

| Optionality | Cardinality | Obligation |
|:-----------| :------------: | :-----------: |
| Mandatory | 1.. | handle |
| Required | 0.. | handle |
| Optional | 0.. | x |


### MyHealth@EU Logical Models

Logical models representing the data sets specified in the MyHealth@EU Requirements Catalogue

| Name | Description |
|:-----------| :------------ |
| [1 Laboratory Result Content](StructureDefinition-Idr.html) | Imaging Diagnostic Report Content as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.1 Patient/subject](StructureDefinition-Subject.html) | Identification of the patient/subject and Patient/subject related contact information as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.2 Health insurance and payment information](StructureDefinition-Payer.html) | Health insurance and payment information as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.3 Information recipient](StructureDefinition-Recipient.html) | Information recipient as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.4 Author](StructureDefinition-Author.html) | Author as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.5 Legal authenticator](StructureDefinition-LegalAuthenticator.html) | Legal authenticator as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.6 Result validator](StructureDefinition-Validator.html) | Result validator as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.7 Order information and reason](StructureDefinition-Order.html) | Order information and reason as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.8 Specimen information](StructureDefinition-SpecimenLab.html) | Specimen information as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |
| [1.9 Results data elements](StructureDefinition-SpecimenLab.html) | Results data elements as defined by the MyHealth@EU business requirement ‘13.01 Create the MyHealth@EU Laboratory Result Report Content’. |