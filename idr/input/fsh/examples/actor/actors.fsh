Instance: actor-handler-eu-lab
InstanceOf: ActorDefinition
Title: "Actor Imaging Diagnostic Report Handler"
Description: """Imaging Diagnostic Report Handler: a generic system dealing with Imaging Diagnostic Reports"""
Usage: #example

* url = "http://hl7.eu/fhir/laboratory/actor-handler-eu-lab"
* name = "IdrHandler"
* title = "Imaging Diagnostic Report Handler"
* status = #active
* type = #system
// * type = #entity
* documentation = """This actor represents a generic system dealing with Imaging Diagnostic Reports."""



Instance: actor-creator-eu-lab
InstanceOf: ActorDefinition
Title: "Actor Imaging Diagnostic Report Creator"
Description: """Imaging Diagnostic Report Creator: a system generating and sending/providing a Imaging Diagnostic Report to a Consumer or to a Repository for report storage and sharing."""
Usage: #example

* url = "http://hl7.eu/fhir/laboratory/actor-creator-eu-lab"
* name = "IdrCreator"
* title = "Imaging Diagnostic Report Creator"
* status = #active
* type = #system
// * type = #entity
* documentation = """This actor represents the Imaging Diagnostic Report creators. That is the actor creating the report that can be send to a consumer or to a repository for report storage and sharing."""

Instance: actor-consumer-eu-lab
InstanceOf: ActorDefinition
Title: "Actor Imaging Diagnostic Report Consumer"
Description: """Imaging Diagnostic Report Report Consumer: a system receiving/querying and using a Imaging Diagnostic Report."""
Usage: #example

* url = "http://hl7.eu/fhir/laboratory/actor-consumer-eu-lab"
* name = "IdrConsumer"
* title = "Imaging Diagnostic Report Consumer"
* status = #active
* type = #system
// * type = #entity
* documentation = """This actor represents the Imaging Diagnostic Report consumers. That is the system using the report received or retrieved.
Using includes, but is not limited to, the report display, storage or processing."""

Instance: actor-repos-eu-lab
InstanceOf: ActorDefinition
Title: "Actor Imaging Diagnostic Report Repository"
Description: """Imaging Diagnostic Report Report Repository: a system maintaining a copy of the received Imaging Diagnostic Report, to store and make it availaìble for the consumers."""
Usage: #example

* url = "http://hl7.eu/fhir/laboratory/actor-repos-eu-lab"
* name = "IdrRepos"
* title = "Imaging Diagnostic Report Repository"
* status = #active
* type = #system
// * type = #entity
* documentation = """This actor represents the Imaging Diagnostic Report Repositories. That is any system maintaining a copy of the report received, to store and make it availaìble for the consumers."""