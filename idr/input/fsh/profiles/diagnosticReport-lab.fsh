Profile: DiagnosticReportIdrMyHealthEu
Parent: DiagnosticReport // $DiagnosticReport-eu-lab
Id: DiagnosticReport-idr-myhealtheu
Title: "DiagnosticReport: IDR"
Description: """This profile defines how to constrain the DiagnosticReport resource to represent a Imaging Diagnostic Report for the purpose of this guide."""

* . ^short = "Imaging Diagnostic Report DiagnosticReport"
* . ^definition = "Imaging Diagnostic Report DiagnosticReport"

/* * extension contains $diagnostic-report-composition-r5 named DiagnosticReportCompositionR5 1..1
* extension[DiagnosticReportCompositionR5].valueReference only Reference(CompositionIdrMyHealthEu) */

// * insert ReportCategoryRule

* language from EHDSILanguage
* basedOn only Reference (ServiceRequestIdrMyHealthEu)
* code from EHDSILaboratoryReportTypes
* subject only Reference (PatientMyHealthEu)
// * specimen only Reference (SpecimenMyHealthEu)
* result only Reference (ObservationResultsIdrMyHealthEu)
* performer only Reference ( PractitionerMyHealthEu or PractitionerRoleMyHealthEu or Organization)
* resultsInterpreter only Reference ( PractitionerMyHealthEu or PractitionerRoleMyHealthEu or Organization)
* presentedForm ^short = "Entire report as issued (pdf)"

// -- Must Support

* code insert ObligationMS
* effectiveDateTime insert ObligationMS
* language insert ObligationMS
* status insert ObligationMS
* identifier insert ObligationMS
* basedOn insert ObligationMS
* subject insert ObligationMS
// * category[studyType] insert ObligationMS
* specimen insert ObligationMS
* presentedForm insert ObligationMS
* media insert ObligationMS

// * text insert ObligationMS
* conclusion insert ObligationMS
* result insert ObligationMS
