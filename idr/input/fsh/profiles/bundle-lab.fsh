Profile: BundleIdrMyHealthEu
Parent: Bundle // $Bundle-eu-lab
Id: Bundle-idr-myhealtheu
Title: "Bundle: IDR"
Description: """This profile defines how to use a Bundle to represent a Laboratory Result Report in HL7 FHIR for the purpose of this guide."""


* identifier 1.. 
  * ^short = "Instance identifier"
* type = #document (exactly)
* timestamp 1.. 
  * ^short = "Instance identifier"
* link ..0
* entry 1..
* entry ^slicing.discriminator[0].type = #type
* entry ^slicing.discriminator[=].path = "resource"
* entry ^slicing.discriminator[+].type = #profile
* entry ^slicing.discriminator[=].path = "resource"
* entry ^slicing.rules = #open
* entry ^short = "Entry resource in the Hospital Discharge Report bundle"
* entry ^definition = "An entry resource included in the Hospital Discharge Report document bundle resource."
* entry ^comment = "Must contain the HDR Composition as the first entry (only a single Composition resource instance may be included).  Additional constraints are specified in the HDR Composition profile."
* entry.fullUrl 1..
* entry.search ..0
* entry.request ..0
* entry.response ..0

* entry.resource 1..
* entry contains
    composition 1..1 and
    diagnosticReport 0..* and
    patient 1..1 and
    serviceRequest 0..* and
    observation 0..* and
    practitioner 0..* and
    practitionerRole 0..* and
    bodyStructure 0..* and
    device 0..*    

* entry[composition].resource only CompositionIdrMyHealthEu
* entry[diagnosticReport].resource only DiagnosticReportIdrMyHealthEu
* entry[patient] 1..
* entry[patient].resource only PatientMyHealthEu
* entry[serviceRequest].resource only ServiceRequestIdrMyHealthEu
* entry[observation].resource only   ObservationResultsIdrMyHealthEu
* entry[serviceRequest].resource only ServiceRequestIdrMyHealthEu
* entry[practitioner].resource only PractitionerMyHealthEu
* entry[practitionerRole].resource only PractitionerRoleMyHealthEu
* entry[bodyStructure].resource only BodyStructureMyHealthEu
* entry[device].resource only DeviceMeasuringMyHealthEu