#!/bin/bash

echo "Publishing laboratory"
cd laboratory && source _updatePublisher.sh -y && source _genonce.sh && cd ..

echo "Publishing ncp-api"
cd ncp-api && source _updatePublisher.sh -y && source _genonce.sh && cd ..

tail -f /dev/null