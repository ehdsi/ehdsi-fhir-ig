FROM eclipse-temurin:11-alpine
LABEL authors="Kim Wauters"

RUN apk add --no-cache --upgrade bash
RUN apk add --update curl
RUN apk add --update npm
RUN apk add --update jekyll
RUN npm install -g fsh-sushi

COPY laboratory /builds/ehdsi/ehdsi-fhir-ig/laboratory/
COPY ncp-api /builds/ehdsi/ehdsi-fhir-ig/ncp-api/
COPY publish.sh /builds/ehdsi/ehdsi-fhir-ig/

WORKDIR /builds/ehdsi/ehdsi-fhir-ig

RUN chmod +x laboratory/_updatePublisher.sh
RUN chmod +x laboratory/_genonce.sh

#CMD ["sh", "-c", "_updatePublisher.sh -y && _genonce.sh"]
ENTRYPOINT ["sh", "publish.sh"]