# eHDSI-FHIR IG

## Getting started

TODO 

## Prerequisites
- [ ] Have Java 11 installed and accessible by the command line (add it to your PATH)
- Have NPM installed (best to use NVM, node version manager)
- [ ] Install Sushi
- [ ] Install Jekyll

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://code.europa.eu/ehdsi/ehdsi-fhir-ig.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://code.europa.eu/ehdsi/ehdsi-fhir-ig/-/settings/integrations)


## Test and Deploy
- Run laboratory/_updatePublisher (.bat for windows, .sh for linux and mac)
- Run laboratory/_genonce (.bat for windows, .sh for linux and mac)
***
