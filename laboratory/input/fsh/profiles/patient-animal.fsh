Profile:  PatientAnimalMyHealthEu
Parent: PatientAnimalEu
Id: Patient-animal-lab-myhealtheu
Title:    "Patient: Animal"
Description: """This profile defines how to represent an Animal as subject of care in FHIR for the purpose of this guide.
This is used to identify the species when a specimen is collected from an animal"""

* extension[patient-animal].extension[species].valueCodeableConcept from EHDSIAnimalSpeciesWithExceptions
