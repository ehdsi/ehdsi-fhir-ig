Instance: SampleSpecimenAnimal
InstanceOf: SpecimenMyHealthEu
Title: "Specimen: animal subject"
Description: "Specimen: example of animal subject"
Usage: #example
* contained = SamplePatientAnimal
* status = #available
* type = $sct#119297000	"Blood specimen"
* subject = Reference(SamplePatientAnimal)
* collection.collectedDateTime = "2022-10-25T13:35:00+01:00"

Instance: SamplePatientAnimal
InstanceOf: PatientAnimalMyHealthEu
Title: "Patient: animal"
Description: "Patient: animal"
Usage: #inline
* extension[$patient-animal].extension[species].valueCodeableConcept = $sct#448169003 "Domestic cat"