
### Overview 

This guide:
* specifies a set of rules to be applied to HL7 FHIR to define how to represent a **Laboratory Report** for the **European cross-borders exchange**, coherently with the European eHN Guidelines (see the [European eHealth - Key documents](https://health.ec.europa.eu/ehealth-digital-health-and-care/key-documents_en) ).
* provides a representation of the Laboratory Result Content specified in the MyHealth@EU Functional Requirements [13.01 Create the MyHealth@EU Laboratory Result Report Content](https://webgate.ec.europa.eu/fpfis/wikis/display/EHDSI/13.01+Create+the+MyHealth@EU+Laboratory+Result+Report+Content) expressed as HL7 FHIR Logical Model.
* is derived for the [HL7 Europe Laboratory Report](https://hl7.eu/fhir/laboratory/) HL7 FHIR IG fulfilling the MyHealth@EU Functional Requirements. More details in the [MyHealth@EU Requirements](requirements.html) page.


The following figure shows how the eHN guidelines, MyHealth@EU Functional Requirements and HL7 FHIR specifications are documented thorugh the different used HL7 FHIR IGs

<div align="center">
<img src="eHN-to-specs.png"  alt="From guidelines to specifications" width="65%">
<p>Figure 1 - From the eHN guidelines to the MyHealth@EU specifications</p>
<p></p>
</div>

The eHN guidelines is documented in the [HL7 Europe Laboratory Report](https://hl7.eu/fhir/laboratory/) as HL7 FHIR Logical Models, including how this data set is implemented in HL7 FHIR; while this guide documents cross-borders reuqirements and associated profiles.



### What is in

This guide includes three main kinds of content (see figure below)
1. HL7 FHIR Logical Models representing the MyHealth@EU Functional Requirements. Logical Models are not supposed to be implemented and exchanged.
1. HL7 FHIR Conformance Resources (e.g. profiles, value sets) describing how to implement HL7 FHIR for the purpose of this guide.
1. HL7 FHIR ConceptMap describing models to profiles forward mapping.

<div align="center">
<img src="guide-content.png"  alt="Guide Content" width="65%">
<p>Figure 2 - Guide Content</p>
<p></p>
</div>

The design choices adopted for representing MyHealth@EU Functional Requirements are describe in the [MyHealth@EU Requirements](requirements.html) page; those used for HL7 FHIR Conformance Resources and ConceptMap below

### Conformance Resources

Most of the design choices adopted by this guide are detailed in the parent guide [HL7 EU Laboratory Report Design choices page](https://hl7.eu/ig/hl7-eu/laboratory/design-choice.html).

A detailed description on how to read HL7 FHIR Implementation Guides is provided in the [Reading Implementation Guides](https://build.fhir.org/ig/FHIR/ig-guidance/reading) guide.

#### Flagging elements

This guide **doesn't use the Must Support flag**, however, to highlight the elements realizing the fields identified by the MyHealth@EU Functional Requirements a generic [*handle*](https://hl7.org/fhir/tools/0.1.0/CodeSystem-obligation.html#obligation-handle)  obligation is used in the profile.

For the purpose of this guide *handle* elements are supposed to be:
* displayed when received by a displaying systems
* transcoded/translated as needed if the element is a Coding or a CodeableConcept

More deatiled Obligations will be defined in the future, a first example of detailed obligations is given in the [Obligations page](obligations.html)


#### Representing functional requirements

Implementers should be aware about the differences in the design of HL7 FHIR and HL7 CDA.
This explain the different apporach followed on the way mandatory, required, or optional elements are represented in the implemantable specification.

In HL7 CDA, unless explictly flagged otherwise, elements can be always nullflavored. 
This is not what is expected to be done in general in HL7 FHIR, where there is not a nullFlavor attribute and the "1.." cardinality is supposed to be used only for the mandatory elements.

For this reason required field are not necessarly mapped into "1.." elements, unless it has been decided otherwise and the [data-absent-reason extension](https://hl7.org/fhir/R4/extension-data-absent-reason.html) used.

Obligations are expected to be adopted to enforce the functional requirements; the inclusion of warning invariants may also be considered in the future to highlight missing *required* elements.

###  Models to profiles forward mapping

This guide uses ConceptMap to formalize the models to profiles forward mapping.

Author are aware that ConceptMaps have been designed for managing the termonology concpet maps; but it is recognized that they may be used for mapping concepts model-to-model, when the scope is not a structural mapping (for which StrcutureMap shoudl be used).

This choice has been made to:
* decouple the mapping from the models
* specifying the kind of Relationship between the concepts (e.g. if concepts are equivalent or only somehow related )