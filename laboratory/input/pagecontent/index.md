{% include fsh-link-references.md %}

<div xmlns="http://www.w3.org/1999/xhtml"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <blockquote class="stu-note">  
  <p>Obligations have been added to this version of the guide only as <b>Informative</b> material to collect feedback about their usage.</p>  
  <p>For more details about obligations please refer to the <a href="obligations.html">Obligations page</a></p>
 </blockquote>
</div>

### Scope

Specify a set of rules to be applied to HL7 FHIR to define how to represent a **Laboratory Report** for the **European cross-borders exchange**, coherently with the European eHN Guidelines (see the [European eHealth - Key documents](https://health.ec.europa.eu/ehealth-digital-health-and-care/key-documents_en) ).
 
This Implementation Guide applies to laboratory reports within the core fields of in-vitro diagnostics (e.g. clinical biochemistry, haematology, microbiology, ...), while leaving out some specialised laboratory domains like histopathology or medical genetics.
 
This guide is derived for the [HL7 Europe Laboratory Report](https://hl7.eu/fhir/laboratory/) HL7 FHIR IG fulfilling the MyHealth@EU Functional Requirements (see the [MyHealth@EU Requirements](requirements.html) page)
 
### MyHealth@EU

The Use Case originates from the Change Proposal produced by the X-eHealth project/MyHealth@EU New Use Case Workgroup, and the details are available in the corresponding Change Proposal: CP-eHealthDSI-073: Implement the new service Laboratory Result Report (corresponding to the Wave 8 of MyHealth@EU).

The corresponding eHN Guidelines: eHN Laboratory Result Guidelines (release 1) are available [here](https://health.ec.europa.eu/publications/ehn-laboratory-result-guidelines-release-1_en)

### Design choices

The solution adopted by this guide - and detailed in the [HL7 EU Laboratory Report Design choices page](https://hl7.eu/ig/hl7-eu/laboratory/design-choice.html) - balances the business requirement of Laboratory Report as legally signable document (i.e. as a HL7 FHIR document), with the expectation to get Lab Report by searching per DiagnosticReport. All this, taking into account the R5 DiagnosticReport design pattern where the DiagnosticReport - Composition relationship is directed from the DiagnosticReport to the Composition resource.

This is done by supporting both perspectives (see figure below) requiring the document bundle ( [BundleLabReportMyHealthEu] ) to always include a DiagnosticReport ( [DiagnosticReportLabMyHealthEu] ) and enabling the pre-adoption of the R5 rules for the inclusion of entries in the Document Bundle.

<div>
<img src="lab-structure.png"  alt="Laboratory report design approach" width="50%">
<p>Figure 1 - Overview of the report design approach</p>
<p></p>
</div>

### Navigating the profiles

The following diagrams provide a browseable overview of the profiles specified by this guide (not all the relationships have been reported).

The first highlights the most relevant relationships starting from the DiagnosticReport ( [DiagnosticReportLabMyHealthEu] ) resource (REST Perspective).

<div>
<p>{% include links-overview.svg %}</p>
<p>Figure 2 - Overview of the profiles relationships</p>
<p></p>
</div>

The second the profiles included in the document bundle ( [BundleLabReportMyHealthEu] ) (Document Perspective).

<div>
<p>{% include document-overview.svg %}</p>
<p>Figure 3 - Overview of the document structure</p>
<p></p>
</div>

### Dependencies

{% include dependency-table.xhtml %}

### Cross Version Analysis

{% include cross-version-analysis.xhtml %}

### Global Profiles

{% include globals-table.xhtml %}

### IP statements

{% include ip-statements.xhtml %}
